## [0.10]() (11.08.2018)

* Обновил annotation-hydrator до 0.9 (фикс критических мест связанных с кешированием)

### Bug Fixes

* **\Vinds\AnnotationHydratorBitrix\Strategies\FileStrategy::fetchFiles** - для ключа теперь используется уникальный идентификатор контекста

## [0.9.2]() (11.08.2018)

* Обновил annotation-hydrator до 0.8.1 (фикс критических мест связанных с кешированием)
* Админка - Список полей - контрол для включения/отключения всех полей
* Мелкие правки

## [0.9]() (05.08.2018)

* Обновлен angular
* **\Vinds\AnnotationHydratorBitrix\Strategies\FileStrategy::extract** - вслучае если значение пустое возвращает массив для удаления файла: `['del' => 'Y']`
* **\Vinds\AnnotationHydratorBitrix\Types\FileMetadata** - класс для получения данных о файле
* **\Vinds\AnnotationHydratorBitrix\Types\File::getMetadata()** - получение класса с данными о файле

## [0.8.2]() (23.06.2018)

* Обновлены зависимости
* **\Vinds\AnnotationHydratorBitrix\Types\File::getSrc** - получение пути до файла

## [0.8]() (04.06.2018)

### BREAKING CHANGES

* **Strategies\FileStrategy::setEntityManager** - метод удален
* **Strategies\IBlock\EnumStrategy::setEnumRepository** - метод удален
* **Strategies\UserField\EnumStrategy::setEnumRepository** - метод удален

### Bug Fixes

* **AdminSection\IBlockBuilder** - передаваемый тип теперь меняется у поля
* **AdminSection\HigloadBlockBuilder** - передаваемый тип теперь меняется у поля

### Features

* **DI**
* Поля типа 'Привязка к элементу' `\Vinds\AnnotationHydrator\Annotations\ReferenceField`
* Опции для полей типа 'Привязка к элементу' через пользовательский интерфейс


## [0.7.1]() (19.05.2018)

### Bug Fixes

* **AdminSection\ClassBuilder** - тип множественных полей

### Features

* **Список инфоблоков** - поиск
* **Список хайлоадов** - поиск

## [0.7]() (10.05.2018)

### Features

* **/bitrix/admin/annotation-hydrator/** - Страница для создание классов инфоблоков и хайлоада, на основе их полей
* **Annotations\UserField\EnumField** - аннотация для пользовательских полей типа "Список"

### BREAKING CHANGES

* **Types\IBlock** - переименован в Types\Enum

## [0.6]() (23.04.2018)

### Changes

* Запись и извлечения данных переписаны на стратегии

## [0.5.1]() (23.04.2018)

### BREAKING CHANGES

* **IBlockId** - аннотация для указания id инфоблока теперь можно указывать только у классов

## [0.5]() (22.04.2018)

### Features

* **IBlockId** - аннотация для указания id инфоблока
* **EnumField** - аннотация для свойств инфоблока типа "Список"
* **BitrixCache** - добавлена возможность указывать теги для кэшируемых данных
