<?php


namespace Vinds\AnnotationHydratorBitrix\Tests\EnumFieldTest;
use Vinds\AnnotationHydrator\Annotations\Entity;

/**
 * @Entity()
 * @\Vinds\AnnotationHydratorBitrix\Annotations\IBlock\IBlockId(1)
 * Class EntityTest
 * @package Vinds\AnnotationHydratorBitrix\Tests\EnumFieldTest
 */
class EntityTest {

    /**
     * @\Vinds\AnnotationHydratorBitrix\Annotations\IBlock\Property()
     * @\Vinds\AnnotationHydratorBitrix\Annotations\IBlock\EnumField(name="ENUM_ITEM")
     *
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem
     */
    public $enumItem;

}