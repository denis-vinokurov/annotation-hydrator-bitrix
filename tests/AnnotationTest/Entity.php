<?php

namespace Vinds\AnnotationHydratorBitrix\Tests\AnnotationTest;

use Vinds\AnnotationHydrator\Annotations\StringField;
use Vinds\AnnotationHydratorBitrix\Annotations\DateTimeField;
use Vinds\AnnotationHydratorBitrix\Annotations\BooleanField;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock\Property;

/**
 * @\Vinds\AnnotationHydrator\Annotations\Entity()
 * Class Entity
 * @package Vinds\AnnotationHydratorBitrix\Tests\AnnotationTest
 */
class Entity {

    /**
     * @DateTimeField(name="DATE_TIME")
     * @var \DateTime
     */
    public $dateTime;

    /**
     * @BooleanField(name="BOOLEAN")
     * @var bool
     */
    public $boolean;

    /**
     * @BooleanField(name="BOOLEAN_FALSE")
     * @var bool
     */
    public $booleanFalse;

    /**
     * @Property()
     * @StringField(name="PROPERTY")
     * @var string
     */
    public $property;
}