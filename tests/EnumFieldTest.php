<?php


namespace Vinds\AnnotationHydratorBitrix\Tests;


use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydratorBitrix\BitrixCache;
use Vinds\AnnotationHydratorBitrix\Tests\EnumFieldTest\EntityTest;
use \Vinds\AnnotationHydratorBitrix\Types;
use \Vinds\AnnotationHydratorBitrix\IBlock;
use PHPUnit\Framework\TestCase;
use Vinds\AnnotationHydratorBitrix\Types\Enum;
use Zend\ServiceManager\ServiceManager;

class EnumFieldTest extends TestCase {

    /**
     * @var EntityManager
     */
    protected $entityManager;
    protected $container;

    /**
     */
    protected function setUp() {
        parent::setUp();

        $config = require __DIR__ . '/config.php';

        $config['factories'][IBlock\EnumRepository::class] = function (ContainerInterface $container) {
            return new class($container->get(Enum\EnumFactory::class), new BitrixCache('/enum-repository/')) extends IBlock\EnumRepository {
                public function get($iBlockId, $code): Enum {
                    $factory = $this->enumFactory;
                    return $factory([
                        [
                            'ID' => 1,
                            'XML_ID' => 'XML_ID_1',
                            'VALUE' => 'Значение 1',
                            'SORT' => 1
                        ],
                        [
                            'ID' => 2,
                            'XML_ID' => 'XML_ID_2',
                            'VALUE' => 'Значение 2',
                            'SORT' => 2
                        ],
                        [
                            'ID' => 3,
                            'XML_ID' => 'XML_ID_3',
                            'VALUE' => 'Значение 3',
                            'SORT' => 3
                        ],
                        [
                            'ID' => 4,
                            'XML_ID' => 'XML_ID_4',
                            'VALUE' => 'Значение 4',
                            'SORT' => 4
                        ],
                    ]);
                }
            };
        };
        $this->container = new ServiceManager($config);
        $this->entityManager = new EntityManager($this->container);
    }

    public function testHydrate() {
        /** @var EntityTest $entity */
        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'PROPERTY_ENUM_ITEM_VALUE' => 'Значение 3'
        ]);

        $this->assertEquals($entity->enumItem->getId(), 3);
        $this->assertEquals($entity->enumItem->getValue(), 'Значение 3');
        $this->assertEquals($entity->enumItem->getXmlId(), 'XML_ID_3');
        $this->assertEquals($entity->enumItem->getSort(), 3);


        /** @var EntityTest $entity */
        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'PROPERTY_ENUM_ITEM_ENUM_ID' => 2,
            'PROPERTY_ENUM_ITEM_VALUE' => 'Значение 2',
        ]);

        $this->assertEquals($entity->enumItem->getId(), 2);
        $this->assertEquals($entity->enumItem->getValue(), 'Значение 2');
        $this->assertEquals($entity->enumItem->getXmlId(), 'XML_ID_2');
        $this->assertEquals($entity->enumItem->getSort(), 2);
    }

    /**
     * @throws \ReflectionException
     */
    public function testExtract() {
        $entity = new EntityTest();
        $factory = new Types\Enum\EnumItemFactory();
        $entity->enumItem = $factory(
            [
                'ID' => 4,
                'XML_ID' => 'XML_ID_4',
                'VALUE' => 'Значение 4',
                'SORT' => 4
            ]
        );

        $extractData = $this->entityManager->extract($entity);

        $this->assertEquals($extractData['PROPERTY_ENUM_ITEM_VALUE'], 4);
    }
}