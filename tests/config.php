<?php

namespace Vinds\AnnotationHydratorBitrix;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\Cache;
use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydratorBitrix\Types\Enum\EnumFactory;
use Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItemFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'factories' => [
        \Doctrine\Common\Annotations\Reader::class => function() {
            return new CachedReader(
                new AnnotationReader(),
                new \Doctrine\Common\Cache\ArrayCache(),
                true
            );
        },

        Cache::class => function() {
            return new BitrixCache();
        },

        IBlock\EnumRepository::class => function(ContainerInterface $container) {
            return new IBlock\EnumRepository($container->get(Types\Enum\EnumFactory::class), new BitrixCache());
        },

        EnumFactory::class => function(ContainerInterface $container) {
            return new EnumFactory($container->get(EnumItemFactory::class));
        },

        EnumItemFactory::class => InvokableFactory::class,

        EntityManager::class => function(ContainerInterface $container) {
            return new EntityManager($container);
        },
    ]
];