<?php

namespace Vinds\AnnotationHydratorBitrix\Tests;

use PHPUnit\Framework\TestCase;
use Vinds\AnnotationHydratorBitrix\BitrixCache;

class BitrixCacheTest extends TestCase {

    /**
     * @var BitrixCache
     */
    protected $cache;

    public function setUp() {
        $this->cache = new BitrixCache();
    }

    public function testSave() {
        $key = md5(__METHOD__);
        $data = __METHOD__;

        $this->cache->delete($key);
        $this->cache->save($key, $data);

        $this->assertTrue(file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/cache/doctrine/{$key}/"));
    }

    public function testContains() {
        $key = md5(__METHOD__);
        $data = __METHOD__;

        $this->cache->delete($key);
        $this->cache->save($key, $data);


        $this->assertTrue($this->cache->contains($key));
    }

    public function testFetch() {
        $key = md5(__METHOD__);
        $data = __METHOD__;

        $this->cache->delete($key);
        $this->cache->save($key, $data);


        $cached = $this->cache->fetch($key);

        $this->assertTrue($cached == $data);
    }

    public function testDelete() {
        $key = md5(__METHOD__);
        $data = __METHOD__;

        $this->cache->save($key, $data);

        $this->cache->delete($key);

        $this->assertTrue(!file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/cache/doctrine/{$key}/"));
    }
}
