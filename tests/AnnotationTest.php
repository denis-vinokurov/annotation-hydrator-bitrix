<?php

namespace Vinds\AnnotationHydratorBitrix\Tests;


use Bitrix\Main\Type\DateTime;
use PHPUnit\Framework\TestCase;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydratorBitrix\Tests\AnnotationTest\Entity;
use Zend\ServiceManager\ServiceManager;

class AnnotationTest extends TestCase {


    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $container;

    /**
     */
    protected function setUp() {
        parent::setUp();
        /** @var \CMain $APPLICATION */
        global $APPLICATION;
        $APPLICATION->RestartBuffer();

        $this->container = new ServiceManager(require __DIR__ . '/config.php');
        $this->entityManager = new EntityManager($this->container);
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreateEntity() {

        $date = (new \DateTime());

        /** @var AnnotationTest\Entity $entity */
        $entity = $this->entityManager->hydrate(AnnotationTest\Entity::class, [
            'DATE_TIME' => $date->format(DateTime::getFormat()),
            'BOOLEAN' => 'Y',
            'BOOLEAN_FALSE' => 'N',
        ]);

        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals($entity->dateTime->format(\DateTime::ISO8601), $date->format(\DateTime::ISO8601));
        $this->assertEquals($entity->boolean, true);
        $this->assertEquals($entity->booleanFalse, false);

        // Проверка создание пустого объекта
        $entity = $this->entityManager->hydrate(AnnotationTest\Entity::class, []);

        $this->assertNull($entity->dateTime);
        $this->assertNull($entity->boolean);
        $this->assertNull($entity->booleanFalse);


        // Проверка оьъекта с пустыми значениями
        $entity = $this->entityManager->hydrate(AnnotationTest\Entity::class, [
            'DATE_TIME' => null,
            'BOOLEAN' => null,
            'BOOLEAN_FALSE' => null,
        ]);

        $this->assertNull($entity->dateTime);
        $this->assertFalse($entity->boolean);
        $this->assertFalse($entity->booleanFalse);
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreateProperty() {
        /** @var Entity $entity */
        $entity = $this->entityManager->hydrate(Entity::class, [
            'PROPERTY_PROPERTY_VALUE' => 'create'
        ]);
        $this->assertEquals($entity->property, 'create');
    }

    /**
     * @throws \ReflectionException
     */
    public function testExtractProperty() {
        $entity = new Entity();
        $entity->property = 'extract';
        $data = $this->entityManager->extract($entity);
        $this->assertEquals($data['PROPERTY_PROPERTY_VALUE'], 'extract');
    }
}
