<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
$_SERVER["DOCUMENT_ROOT"] = __DIR__ . '/../html';
require(__DIR__ . "/../html/bitrix/modules/main/include/prolog_before.php");
$loader = require __DIR__ . '/../vendor/autoload.php';

AnnotationRegistry::registerLoader([$loader, 'loadClass']);
