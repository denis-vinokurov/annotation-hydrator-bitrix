# Страница создание классов /bitrix/admin/annotation-hydrator.php

Для вывода страницы необходимо сделать сделать следующие:

* в файле `/local/php_interface/init.php` подключить файл `require_once $PATH_TO_PACKAGE . '/bitrix-files/menu.php'`
* создать файл `/bitrix/admin/annotation-hydrator.php` с подключением файла `require $PATH_TO_PACKAGE. '/bitrix-files/admin-section.php';`
* после этого страница будет доступна по пути `Сервисы->Annotation hydrator`

# Поля типа 'Привяка к элементу'
Репозитории должны реализовывать интерфейс `\Vinds\AnnotationHydrator\Repository\RepositoryInterface` и быть зарегистрированный в `\Psr\Container\ContainerInterface` по следующим шаблонам:
* Репозитории элементов инфоблока - `iBlockElement#IBLOCK_ID#` 
* Репозитории разделов инфоблока - `iBlockSection#IBLOCK_ID#` 
* Репозитории элементов хайлоада - `HighLoadBlock#HIGHLOAD_ID#`

# FileField
Перед выборкой файлов из базы, если есть контекст коллекции выбираются все id файлов коллекции и затем получаются 1 запросов. В результате делается только 1 запрос к таблице b_file.

Перед использованием данного типа поля необходимо установить `\Vinds\AnnotationHydrator\EntityManager` в поля `\Vinds\AnnotationHydratorBitrix\Annotations\FileField`. `\Vinds\AnnotationHydrator\EntityManager` будет использоваться для выборки остальных полей с файлами

```php
$entityManager = $locator->get(\Vinds\AnnotationHydrator\EntityManager::class);
\Vinds\AnnotationHydratorBitrix\Annotations\FileField::setEntityManager($entityManager);
```

# IBlock

##\Vinds\AnnotationHydratorBitrix\Annotations\IBlock\IBlockId
#####РЕКОМЕНДУЕТСЯ УКАЗЫВАТЬ У ВСЕХ КЛАССОВ КОТОРЫЕ ОТНОСЯТСЯ К ИНФОБЛОКАМ
Добавляет в опции значение с id инфоблока. можно применять классу 


```php
use Vinds\AnnotationHydrator\Annotations;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock;

/**
 * @Annotations\Entity()
 * @IBlock\IBlockId(3)
 */
class Entity {
    /**
     * @IBlock\Property()
     * @IBlock\EnumField(name="SIZES_CLOTHES")
     *
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem
     */
    protected $sizesClothes;
}
```
или
```php
use Vinds\AnnotationHydrator\Annotations;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock;

define('IB_SKU', 3);

/**
 * @Annotations\Entity()
 * @IBlock\IBlockId(IB_SKU)
 */
class Entity {
    /**
     * @IBlock\Property()
     * @IBlock\EnumField(name="SIZES_CLOTHES")
     *
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem
     */
    protected $sizesClothes;
}
```

##\Vinds\AnnotationHydratorBitrix\Annotations\IBlock\Property
При указании данной аннотации для свойств класса содержащих данные из свойств ИБ, не надо указывать префикс `PROPERTY_` и постфикс `_VALUE`.
Пример:

```php
use Vinds\AnnotationHydrator\Annotations;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock;

/**
 * @Annotations\Entity()
 */
class Entity {
    /**
     * @IBlock\Property()
     * @IBlock\EnumField(name="SIZES_CLOTHES")
     *
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem
     */
    protected $sizesClothes;
}
```

##IBlock\EnumRepository

####Создание репозитория для значений свойств ИБ типа список

```php
use \Vinds\AnnotationHydratorBitrix\Types;
use \Vinds\AnnotationHydratorBitrix\IBlock;

$enumItemFactory = new Types\Enum\EnumItemFactory();

$enumFactory = new Types\Enum\EnumFactory($enumItemFactory);

$cache = new \Vinds\AnnotationHydratorBitrix\BitrixCache('/enum-repository/');

$enumRepository = new IBlock\EnumRepository($enumFactory, $cache);
```

#### Получение списка значений
Выборка списка кэшируется  с помощью `\Vinds\AnnotationHydratorBitrix\BitrixCache` в папку `/enum-repository/`. Кеш живет 1 сутки и помечается тегом `iblock_id_${iBlockId}`

```php
$list = $emumRepository->get($iBlockId, $propertyCode);
```

#HighloadBlock

##\Vinds\AnnotationHydratorBitrix\Annotations\HighloadBlock\HLBlockId
#####РЕКОМЕНДУЕТСЯ УКАЗЫВАТЬ У ВСЕХ КЛАССОВ КОТОРЫЕ ОТНОСЯТСЯ К HIGHLOAD_BLOCK
Добавляет в опции значение с id инфоблока. можно применять классу 


```php
use Vinds\AnnotationHydrator\Annotations;
use Vinds\AnnotationHydratorBitrix\Annotations\HighloadBlock;

/**
 * @Annotations\Entity()
 * @IBlock\HLBlockId(3)
 */
class Entity {
    /**
     * @\Vinds\AnnotationHydratorBitrix\Annotations\UserField\EnumField(name="SIZES_CLOTHES")
     *
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem
     */
    protected $sizesClothes;
}
```

# UserField

##UserField\EnumRepository

####Создание репозитория для значений пользовательских типа список

```php
use \Vinds\AnnotationHydratorBitrix\Types;
use \Vinds\AnnotationHydratorBitrix\UserField;

$enumItemFactory = new Types\Enum\EnumItemFactory();
$enumFactory = new Types\Enum\EnumFactory($enumItemFactory);

$enumRepository = new UserField\EnumRepository($enumFactory);
```

#### Получение списка значений

```php
$list = $enumRepository->get($entityId, $fieldName);
```
