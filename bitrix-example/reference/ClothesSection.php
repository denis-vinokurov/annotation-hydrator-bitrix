<?php
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock\IBlockId;
use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\Primary;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydrator\Annotations\StringField;
use Vinds\AnnotationHydratorBitrix\Annotations\FileField;
use Vinds\AnnotationHydratorBitrix\Annotations\UserField\EnumField;

/**
 * @IBlockId(2)
 * @Entity()
 */
class ClothesSection
{

    /**
     * @Primary()
     * @IntField(name="ID")
     * @var int|null
     */
    protected $id;

    /**
     * @StringField(name="CODE")
     * @var string|null
     */
    protected $code;

    /**
     * @IntField(name="IBLOCK_ID")
     * @var int|null
     */
    protected $iblockId;

    /**
     * @StringField(name="NAME")
     * @var string|null
     */
    protected $name;

    /**
     * @StringField(name="UF_BROWSER_TITLE")
     * @var string|null
     */
    protected $browserTitle;

    /**
     * @StringField(name="UF_KEYWORDS")
     * @var string|null
     */
    protected $keywords;

    /**
     * @StringField(name="UF_META_DESCRIPTION")
     * @var string|null
     */
    protected $metaDescription;

    /**
     * @FileField(name="UF_BACKGROUND_IMAGE")
     * @var \Vinds\AnnotationHydratorBitrix\Types\File|null
     */
    protected $backgroundImage;

    /**
     * @EnumField(name="UF_LIST")
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem|null
     */
    protected $list;


}
