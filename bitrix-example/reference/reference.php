<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Zend\ServiceManager\ServiceManager;
use Vinds\AnnotationHydratorBitrix\Types;
use Vinds\AnnotationHydratorBitrix\IBlock;
use Vinds\AnnotationHydratorBitrix\UserField;

$loader = require __DIR__ . '/../vendor/autoload.php';

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader([$loader, 'loadClass']);

abstract class AbstractList implements \Vinds\AnnotationHydrator\Repository\ListInterface {

    protected $entityClass;
    protected $id;

    protected $where = [];
    /**
     * @var \Vinds\AnnotationHydrator\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Vinds\AnnotationHydratorBitrix\EntityHelper
     */
    protected $entityHelper;


    public function __construct($id, $entityClass, \Psr\Container\ContainerInterface $container) {
        $this->id          = $id;
        $this->entityClass = $entityClass;
        $this->entityManager = $container->get(\Vinds\AnnotationHydrator\EntityManager::class);
        $this->entityHelper = $container->get(\Vinds\AnnotationHydratorBitrix\EntityHelper::class);
    }

    public function __clone() {
        $this->where = [];
    }

    /**
     * @param mixed $where
     * @param bool $clear
     * @return $this
     */
    public function where($where, $clear = false) {
        $this->where = array_merge($this->where, $where);
        return $this;
    }
}

class ElementList extends AbstractList {

    /**
     * @return iterable
     */
    public function fetch() {

        $this->where['IBLOCK_ID'] = $this->id;
        $rs = CIBlockElement::GetList([], $this->where, false, false, $this->entityHelper->getFields($this->entityClass, true, false));

        $collection = [];
        while ($el = $rs->Fetch()) {
            $collection[] = $el;
        }

        return $this->entityManager->hydrateCollection($this->entityClass, $collection);
    }
}

class SectionList extends AbstractList {

    /**
     * @return iterable
     */
    public function fetch() {

        $this->where['IBLOCK_ID'] = $this->id;
        $rs = CIBlockSection::GetList([], $this->where);

        $collection = [];
        while ($el = $rs->Fetch()) {
            $collection[] = $el;
        }

        return $this->entityManager->hydrateCollection($this->entityClass, $collection);
    }
}

class HLList extends AbstractList {

    public function fetch() {
        $data= Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->id)
            ->getDataClass();

        $collection = $data::getList([
                'filter' => $this->where
            ])
            ->fetchAll();

        return $this->entityManager->hydrateCollection($this->entityClass, $collection);
    }
}

class Repository implements \Vinds\AnnotationHydrator\Repository\RepositoryInterface {

    private $list;

    public function __construct($list) {
        $this->list = $list;
    }

    /**
     * @return \Vinds\AnnotationHydrator\Repository\ListInterface
     */
    public function getList(): \Vinds\AnnotationHydrator\Repository\ListInterface {
        return clone $this->list;
    }
}

require 'Clothes.php';
require 'Brand.php';
require 'ClothesRecommend.php';
require 'ClothesSection.php';

$container = new ServiceManager([
    'factories' => [
        \Doctrine\Common\Annotations\Reader::class => function () {


            return new CachedReader(
                new AnnotationReader(),
                new \Doctrine\Common\Cache\ArrayCache(),
                true
            );
        },

        \Doctrine\Common\Cache\Cache::class => function () {
            return new \Doctrine\Common\Cache\ArrayCache();
        },

        \Vinds\AnnotationHydrator\EntityManager::class => function(\Psr\Container\ContainerInterface $container) {
            return new \Vinds\AnnotationHydrator\EntityManager($container);
        },

        \Vinds\AnnotationHydratorBitrix\IBlock\EnumRepository::class => function() {
            $enumItemFactory = new Types\Enum\EnumItemFactory();

            $enumFactory = new Types\Enum\EnumFactory($enumItemFactory);

            $cache = new \Vinds\AnnotationHydratorBitrix\BitrixCache('/enum-repository/');

            return new IBlock\EnumRepository($enumFactory, $cache);
        },

        \Vinds\AnnotationHydratorBitrix\UserField\EnumRepository::class => function() {

            $enumItemFactory = new Types\Enum\EnumItemFactory();
            $enumFactory = new Types\Enum\EnumFactory($enumItemFactory);

            return new UserField\EnumRepository($enumFactory);
        },

        \Vinds\AnnotationHydratorBitrix\EntityHelper::class => ReflectionBasedAbstractFactory::class,

        'iBlockElement2' => function(\Psr\Container\ContainerInterface $container) {
            return new Repository(new ElementList(2, Clothes::class, $container));
        },
        'iBlockElement2Recommend' => function(\Psr\Container\ContainerInterface $container) {
            return new Repository(new ElementList(2, ClothesRecommend::class, $container));
        },
        'iBlockSection2' => function(\Psr\Container\ContainerInterface $container) {
            return new Repository(new SectionList(2, ClothesSection::class, $container));
        },
        'HighLoadBlock2' => function(\Psr\Container\ContainerInterface $container) {
            return new Repository(new HLList(2, Brand::class, $container));
        },
    ]
]);

/** @var Clothes $item */
$items = $container->get('iBlockElement2')->getList()->fetch();
$item = $items[0];

$item->getBrandRef();
$item->getIblockSectionId();
$item->getSections();
$item->getRecommend();

echo 1;