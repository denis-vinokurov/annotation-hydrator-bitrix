<?php
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock\IBlockId;
use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\Primary;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock\EnumField;
use Vinds\AnnotationHydrator\Annotations\FloatField;
use Vinds\AnnotationHydratorBitrix\Annotations\FileField;
use Vinds\AnnotationHydrator\Annotations\StringField;
use Vinds\AnnotationHydratorBitrix\Annotations\IBlock\Property;
use Vinds\AnnotationHydrator\Annotations\Multiple;
use Vinds\AnnotationHydrator\Annotations\ReferenceField;

/**
 * @IBlockId(2)
 * @Entity()
 */
class Clothes
{

    /**
     * @Primary()
     * @IntField(name="ID")
     * @var int|null
     */
    protected $id;

    /**
     * @StringField(name="CODE")
     * @var string|null
     */
    protected $code;

    /**
     * @StringField(name="NAME")
     * @var string|null
     */
    protected $name;

    /**
     * @IntField(name="IBLOCK_ID")
     * @var int|null
     */
    protected $iblockId;

    /**
     * @ReferenceField(name="IBLOCK_SECTION_ID", repository="iBlockSection2",
     * referenceField="ID")
     * @var \Vinds\AnnotationHydrator\Reference\LazyValue|null
     */
    protected $iblockSectionId;

    /**
     * @Property()
     * @StringField(name="TITLE")
     * @var string|null
     */
    protected $title;

    /**
     * @Property()
     * @StringField(name="KEYWORDS")
     * @var string|null
     */
    protected $keywords;

    /**
     * @Property()
     * @StringField(name="META_DESCRIPTION")
     * @var string|null
     */
    protected $metaDescription;

    /**
     * @Property()
     * @Multiple()
     * @ReferenceField(name="BRAND_REF", repository="HighLoadBlock2",
     * referenceField="UF_XML_ID")
     * @var \Vinds\AnnotationHydrator\Reference\LazyValue[]|null
     */
    protected $brandRef;

    /**
     * @Property()
     * @EnumField(name="NEWPRODUCT")
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem|null
     */
    protected $newproduct;

    /**
     * @Property()
     * @EnumField(name="SALELEADER")
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem|null
     */
    protected $saleleader;

    /**
     * @Property()
     * @EnumField(name="SPECIALOFFER")
     * @var \Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem|null
     */
    protected $specialoffer;

    /**
     * @Property()
     * @StringField(name="ARTNUMBER")
     * @var string|null
     */
    protected $artnumber;

    /**
     * @Property()
     * @StringField(name="MANUFACTURER")
     * @var string|null
     */
    protected $manufacturer;

    /**
     * @Property()
     * @Multiple()
     * @StringField(name="MATERIAL")
     * @var string[]|null
     */
    protected $material;

    /**
     * @Property()
     * @StringField(name="COLOR")
     * @var string|null
     */
    protected $color;

    /**
     * @Property()
     * @Multiple()
     * @FileField(name="MORE_PHOTO")
     * @var \Vinds\AnnotationHydratorBitrix\Types\File[]|null
     */
    protected $morePhoto;

    /**
     * @Property()
     * @FloatField(name="FORUM_MESSAGE_CNT")
     * @var float|null
     */
    protected $forumMessageCnt;

    /**
     * @Property()
     * @FloatField(name="BLOG_POST_ID")
     * @var float|null
     */
    protected $blogPostId;

    /**
     * @Property()
     * @FloatField(name="BLOG_COMMENTS_CNT")
     * @var float|null
     */
    protected $blogCommentsCnt;

    /**
     * @Property()
     * @FileField(name="BACKGROUND_IMAGE")
     * @var \Vinds\AnnotationHydratorBitrix\Types\File|null
     */
    protected $backgroundImage;

    /**
     * @Property()
     * @StringField(name="TREND")
     * @var string|null
     */
    protected $trend;

    /**
     * @Property()
     * @Multiple()
     * @ReferenceField(name="SECTIONS", repository="iBlockSection1",
     * referenceField="ID")
     * @var \Vinds\AnnotationHydrator\Reference\LazyValue[]|null
     */
    protected $sections;

    /**
     * @Property()
     * @Multiple()
     * @ReferenceField(name="RECOMMEND", repository="iBlockElement2Recommend",
     * referenceField="ID")
     * @var \Vinds\AnnotationHydrator\Reference\LazyValue[]|null
     */
    protected $recommend;

    /**
     * @return null|\Vinds\AnnotationHydrator\Reference\LazyValue
     */
    public function getIblockSectionId(): ?ClothesSection {
        if ($this->iblockSectionId instanceof \Vinds\AnnotationHydrator\Reference\LazyValue) {
            $this->iblockSectionId = $this->iblockSectionId->get();
        }

        return $this->iblockSectionId;
    }

    /**
     * @return null|\Vinds\AnnotationHydrator\Reference\LazyValue[]
     */
    public function getBrandRef(): ?array {

        if (is_array($this->brandRef) && reset($this->brandRef) instanceof \Vinds\AnnotationHydrator\Reference\LazyValue) {
            $this->brandRef = array_map(function (\Vinds\AnnotationHydrator\Reference\LazyValue $value) {
                return $value->get();
            }, $this->brandRef);
        }

        return $this->brandRef;
    }

    /**
     * @return null|\Vinds\AnnotationHydrator\Reference\LazyValue[]
     */
    public function getSections(): ?array {
        if (is_array($this->sections) && reset($this->sections) instanceof \Vinds\AnnotationHydrator\Reference\LazyValue) {
            $this->sections = array_map(function (\Vinds\AnnotationHydrator\Reference\LazyValue $value) {
                return $value->get();
            }, $this->sections);
        }

        return $this->sections;
    }

    /**
     * @return null|\Vinds\AnnotationHydrator\Reference\LazyValue[]
     */
    public function getRecommend(): ?array {
        if (is_array($this->recommend) && reset($this->recommend) instanceof \Vinds\AnnotationHydrator\Reference\LazyValue) {
            $this->recommend = array_map(function (\Vinds\AnnotationHydrator\Reference\LazyValue $value) {
                return $value->get();
            }, $this->recommend);
        }

        return $this->recommend;
    }


}
