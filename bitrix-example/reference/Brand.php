<?php
use Vinds\AnnotationHydratorBitrix\Annotations\HighloadBlock\HLBlockId;
use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\Primary;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydratorBitrix\Annotations\FileField;
use Vinds\AnnotationHydrator\Annotations\FloatField;
use Vinds\AnnotationHydrator\Annotations\StringField;

/**
 * @HLBlockId(2)
 * @Entity()
 */
class Brand
{

    /**
     * @Primary()
     * @IntField(name="ID")
     * @var int|null
     */
    protected $id;

    /**
     * @StringField(name="UF_NAME")
     * @var string|null
     */
    protected $name;

    /**
     * @FileField(name="UF_FILE")
     * @var \Vinds\AnnotationHydratorBitrix\Types\File|null
     */
    protected $file;

    /**
     * @StringField(name="UF_LINK")
     * @var string|null
     */
    protected $link;

    /**
     * @StringField(name="UF_DESCRIPTION")
     * @var string|null
     */
    protected $description;

    /**
     * @StringField(name="UF_FULL_DESCRIPTION")
     * @var string|null
     */
    protected $fullDescription;

    /**
     * @FloatField(name="UF_SORT")
     * @var float|null
     */
    protected $sort;

    /**
     * @StringField(name="UF_EXTERNAL_CODE")
     * @var string|null
     */
    protected $externalCode;

    /**
     * @StringField(name="UF_XML_ID")
     * @var string|null
     */
    protected $xmlId;


}
