<?php

namespace Vinds\AnnotationHydratorBitrix\Types;

/**
 * Class FileMetadata
 * @package Vinds\AnnotationHydratorBitrix\Types
 */
class FileMetadata {

    /**
     * @var string
     */
    protected $path;

    /**
     * @var
     */
    protected $fileInfo;

    /**
     * FileMetadata constructor.
     * @param string $path
     */
    public function __construct(string $path) {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getFileInfo() {
        if ($this->fileInfo === null) {
            $this->fileInfo = pathinfo($this->path);
        }
        return $this->fileInfo;
    }

    /**
     * @return bool
     */
    public function isSvg(): bool {
        return strtolower($this->getFileInfo()['extension']) === 'svg';
    }

    /**
     * @return bool
     */
    public function isJpg(): bool {
        $extension = strtolower($this->getFileInfo()['extension']);
        return $extension === 'jpg' || $extension === 'jpeg';
    }

    /**
     * @return bool
     */
    public function isPng(): bool {
        return strtolower($this->getFileInfo()['extension']) === 'png';
    }
}