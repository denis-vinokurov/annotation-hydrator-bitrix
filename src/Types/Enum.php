<?php


namespace Vinds\AnnotationHydratorBitrix\Types;


class Enum {

    /**
     * @var Enum\EnumItem[]
     */
    protected $items;

    /**
     * @return Enum\EnumItem[]
     */
    public function getItems(): array {
        return $this->items;
    }

    /**
     * @param $id
     * @return null|Enum\EnumItem
     */
    public function fetchById($id): ?Enum\EnumItem {
        foreach ($this->items as $item) {
            if ($item->getId() == $id) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return null|Enum\EnumItem
     */
    public function fetchByXmlId($xmlId): ?Enum\EnumItem {
        foreach ($this->items as $item) {
            if ($item->getXmlId() == $xmlId) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param $value
     * @return null|Enum\EnumItem
     */
    public function fetchByValue($value): ?Enum\EnumItem {
        foreach ($this->items as $item) {
            if ($item->getValue() == $value) {
                return $item;
            }
        }
        return null;
    }
}