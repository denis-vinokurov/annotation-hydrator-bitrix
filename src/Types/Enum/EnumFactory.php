<?php

namespace Vinds\AnnotationHydratorBitrix\Types\Enum;

use Vinds\AnnotationHydratorBitrix\Types\Enum;

class EnumFactory {

    /**
     * @var \ReflectionClass
     */
    protected $refClass;

    /**
     * @var \ReflectionProperty
     */
    protected $refPropertyItems;

    /**
     * @var EnumItemFactory
     */
    protected $enumItemFactory;


    /**
     * EnumFactory constructor.
     * @param EnumItemFactory $enumItemFactory
     * @throws \ReflectionException
     */
    public function __construct(EnumItemFactory $enumItemFactory) {
        $this->refClass = new \ReflectionClass(Enum::class);

        $this->refPropertyItems = $this->refClass->getProperty('items');
        $this->refPropertyItems->setAccessible(true);

        $this->enumItemFactory = $enumItemFactory;
    }

    /**
     * @param array $data
     * @return Enum
     */
    public function __invoke(array $data): Enum {
        /** @var Enum $result */
        $result = $this->refClass->newInstanceWithoutConstructor();

        $factory = $this->enumItemFactory;
        $items = [];
        foreach ($data as $item) {
            $items[] = $factory($item);
        }

        $this->refPropertyItems->setValue($result, $items);

        return $result;
    }
}