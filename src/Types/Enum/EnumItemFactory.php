<?php

namespace Vinds\AnnotationHydratorBitrix\Types\Enum;

class EnumItemFactory {

    /**
     * @var \ReflectionClass
     */
    protected $refClass;

    /**
     * @var \ReflectionProperty
     */
    protected $refPropertyId;

    /**
     * @var \ReflectionProperty
     */
    protected $refPropertyXmlId;

    /**
     * @var \ReflectionProperty
     */
    protected $refPropertyValue;

    /**
     * @var \ReflectionProperty
     */
    protected $refPropertySort;


    /**
     * EnumItemFactory constructor.
     * @throws \ReflectionException
     */
    public function __construct() {
        $this->refClass = new\ReflectionClass(EnumItem::class);

        $this->refPropertyId = $this->refClass->getProperty('id');
        $this->refPropertyId->setAccessible(true);

        $this->refPropertyXmlId = $this->refClass->getProperty('xmlId');
        $this->refPropertyXmlId->setAccessible(true);

        $this->refPropertyValue = $this->refClass->getProperty('value');
        $this->refPropertyValue->setAccessible(true);

        $this->refPropertySort = $this->refClass->getProperty('sort');
        $this->refPropertySort->setAccessible(true);
    }

    /**
     * @param array $data
     * @return EnumItem
     */
    public function __invoke(array $data): EnumItem {
        /** @var EnumItem $enumItem */
        $enumItem = $this->refClass->newInstanceWithoutConstructor();

        $this->refPropertyId->setValue($enumItem, (int) $data['ID']);
        $this->refPropertyXmlId->setValue($enumItem, (string) $data['XML_ID']);
        $this->refPropertyValue->setValue($enumItem, (string) $data['VALUE']);
        $this->refPropertySort->setValue($enumItem, (int) $data['SORT']);

        return $enumItem;
    }
}