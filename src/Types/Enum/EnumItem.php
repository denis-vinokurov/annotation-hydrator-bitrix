<?php


namespace Vinds\AnnotationHydratorBitrix\Types\Enum;


class EnumItem {

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $xmlId;

    /**
     * @var int
     */
    protected $sort;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue(): string {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getXmlId(): string {
        return $this->xmlId;
    }

    /**
     * @return int
     */
    public function getSort(): int {
        return $this->sort;
    }
}