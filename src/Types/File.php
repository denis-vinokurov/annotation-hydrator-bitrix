<?php

namespace Vinds\AnnotationHydratorBitrix\Types;

use Vinds\AnnotationHydrator\Annotations;


/**
 * @Annotations\Entity()
 * Class File
 * @package Vinds\AnnotationHydratorBitrix\Types
 */
class File {

    /**
     * @var bool
     */
    protected $new;

    /**
     * @var int|string
     */
    protected $path;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var FileMetadata
     */
    protected $metadata;

    /**
     * File constructor.
     * @param $path
     */
    public function __construct($path) {
        $this->new = true;
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isNew() {
        return $this->new;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->data['ID'];
    }

    /**
     * @return int
     */
    public function getTimestampX() {
        return $this->data['TIMESTAMP_X'];
    }

    /**
     * @return string
     */
    public function getModuleId() {
        return $this->data['MODULE_ID'];
    }

    /**
     * @return int
     */
    public function getHeight() {
        return $this->data['HEIGHT'];
    }

    /**
     * @return int
     */
    public function getWidth() {
        return $this->data['WIDTH'];
    }

    /**
     * @return int
     */
    public function getFileSize() {
        return $this->data['FILE_SIZE'];
    }

    /**
     * @return mixed
     */
    public function getContentType() {
        return $this->data['CONTENT_TYPE'];
    }

    /**
     * @return string
     */
    public function getSubDir() {
        return $this->data['SUBDIR'];
    }

    /**
     * @return string
     */
    public function getFileName() {
        return $this->data['FILE_NAME'];
    }

    /**
     * @return string
     */
    public function getOriginalName() {
        return $this->data['ORIGINAL_NAME'];
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->data['DESCRIPTION'];
    }

    /**
     * @return null|string
     */
    public function getSrc(): ?string {
        $src = \CFile::GetFileSRC($this->toArray());

        if (empty($src)) {
            return null;
        }

        return $src;
    }

    /**
     * @return FileMetadata
     */
    public function getMetadata(): FileMetadata {
        if ($this->metadata === null) {
            $this->metadata = new FileMetadata($this->getSrc());
        }

        return $this->metadata;
    }

    /**
     * @return array
     */
    public function toArray() {
        return $this->data;
    }
}