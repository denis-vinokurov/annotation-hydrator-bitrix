<?php


namespace Vinds\AnnotationHydratorBitrix;


use Bitrix\Main\Application;
use Bitrix\Main\SystemException;
use Doctrine\Common\Cache\Cache;

class BitrixCache implements Cache {

    /**
     * @var int
     */
    protected $upTime;

    /**
     * @var int
     */
    protected $missesCount = 0;

    /**
     * @var int
     */
    protected $hitsCount  = 0;
    /**
     * @var \CPHPCache
     */
    protected $phpCache;

    /**
     * @var int
     */
    protected $lifeTime = 86400;

    /**
     * @var string
     */
    protected $dir;

    /**
     * @var string[]
     */
    protected $tags = [];

    /**
     * BitrixCache constructor.
     * @param string $dir
     */
    public function __construct(string $dir = '/doctrine/') {
        $this->phpCache = new \CPHPCache();
        $this->upTime = time();
        $this->dir = $dir;
    }

    /**
     * Fetches an entry from the cache.
     *
     * @param string $id The id of the cache entry to fetch.
     *
     * @return mixed The cached data or FALSE, if no cache entry exists for the given id.
     */
    public function fetch($id) {
        if (! $this->contains($id)) {
            $this->missesCount += 1;

            return false;
        }

        $this->hitsCount += 1;

        return $this->phpCache->GetVars();
    }

    /**
     * Tests if an entry exists in the cache.
     *
     * @param string $id The cache id of the entry to check for.
     *
     * @return bool TRUE if a cache entry exists for the given cache id, FALSE otherwise.
     */
    public function contains($id) {
        return $this->phpCache->InitCache($this->lifeTime, $id, $this->dir . $id . '/');
    }

    /**
     * Puts data into the cache.
     *
     * If a cache entry with the given id already exists, its data will be replaced.
     *
     * @param string $id The cache id.
     * @param mixed $data The cache entry/data.
     * @param int $lifeTime The lifetime in number of seconds for this cache entry.
     *                         If zero (the default), the entry never expires (although it may be deleted from the cache
     *                         to make place for other entries).
     *
     * @return bool TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    public function save($id, $data, $lifeTime = 0) {
        $this->contains($id);
        $this->phpCache->StartDataCache();

        if (!empty($this->tags)) {
            try {
                $taggedCache = Application::getInstance()->getTaggedCache();
            } catch (SystemException $e) {
                return false;
            }

            $taggedCache->startTagCache($this->dir);

            foreach ($this->tags as $tag) {
                $taggedCache->registerTag($tag);
            }

            $taggedCache->endTagCache();
        }

        $this->phpCache->EndDataCache($data);
        $this->tags = [];
        return true;
    }

    /**
     * Deletes a cache entry.
     *
     * @param string $id The cache id.
     *
     * @return bool TRUE if the cache entry was successfully deleted, FALSE otherwise.
     *              Deleting a non-existing entry is considered successful.
     */
    public function delete($id) {
        $this->phpCache->cleanDir($this->dir . $id . '/');
    }

    /**
     * Retrieves cached information from the data store.
     *
     * The server's statistics array has the following values:
     *
     * - <b>hits</b>
     * Number of keys that have been requested and found present.
     *
     * - <b>misses</b>
     * Number of items that have been requested and not found.
     *
     * - <b>uptime</b>
     * Time that the server is running.
     *
     * - <b>memory_usage</b>
     * Memory used by this server to store items.
     *
     * - <b>memory_available</b>
     * Memory allowed to use for storage.
     *
     * @since 2.2
     *
     * @return array|null An associative array with server's statistics if available, NULL otherwise.
     */
    public function getStats() {
        return [
            Cache::STATS_HITS             => $this->hitsCount,
            Cache::STATS_MISSES           => $this->missesCount,
            Cache::STATS_UPTIME           => $this->upTime,
            Cache::STATS_MEMORY_USAGE     => null,
            Cache::STATS_MEMORY_AVAILABLE => null,
        ];
    }

    /**
     * @param string $tag
     */
    public function tag(string $tag) {
        $this->tags[] = $tag;
    }
}