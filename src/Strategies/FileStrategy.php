<?php


namespace Vinds\AnnotationHydratorBitrix\Strategies;


use Bitrix\Main\FileTable;
use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyWithContainerInterface;
use Vinds\AnnotationHydratorBitrix\Annotations\FileField;
use Vinds\AnnotationHydratorBitrix\Types\File;

class FileStrategy implements StrategyWithContainerInterface {

    /**
     * @var array
     */
    private static $cache = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \ReflectionClass
     */
    private $refClass;

    /**
     * @var \ReflectionProperty
     */
    private $refPropertyPath;

    /**
     * @var \ReflectionProperty
     */
    private $refPropertyData;

    /**
     * FileStrategy constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->refClass        = new \ReflectionClass(File::class);
        $this->refPropertyPath = $this->refClass->getProperty('path');
        $this->refPropertyData = $this->refClass->getProperty('data');
        $this->refPropertyPath->setAccessible(true);
        $this->refPropertyData->setAccessible(true);
        $this->entityManager = $container->get(EntityManager::class);
    }

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return null|File
     * @throws \ReflectionException
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?File {
        if ($value === null) {
            return $value;
        }

        $data = $this->fetchFile($value, $fieldMap, $context);

        if (empty($data)) {
            return null;
        }

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->createFileObject($data);
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param File $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return array|bool|null
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        if (empty($value)) {
            return ['del' => 'Y'];
        } elseif (!$value->isNew()) {
            return $value->getId();
        }

        $path = $this->refPropertyPath->getValue($value);

        if (empty($path)) {
            return null;
        }

        return is_array($path) ? $path : \CFile::MakeFileArray($path);
    }



    /**
     * @param $data
     * @return object|File
     * @throws \ReflectionException
     */
    protected function createFileObject($data) {
        $instance = $this->refClass->newInstanceWithoutConstructor();
        $this->refPropertyData->setValue($instance, $data);
        return $instance;
    }

    /**
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \ReflectionException
     */
    protected function fetchFile($value, FieldMap $fieldMap, ?Context $context) {
        $files = $this->fetchFiles($fieldMap, $context);

        if (empty($files)) {
            return FileTable::getByPrimary($value)->fetch();
        }

        return $files[$value];
    }

    /**
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \ReflectionException
     */
    protected function fetchFiles(FieldMap $fieldMap, ?Context $context) {
        $collection = $this->fetchCollection($context);
        if (empty($collection)) {
            return [];
        }

        // Проверяем есть ли кеш для текущей коллекции
        $cacheKey = $context->parent()->getUniqueIdentifier();
        if (isset(self::$cache[$cacheKey])) {
            return self::$cache[$cacheKey];
        }

        // Получаем все поля с типом файл
        $metadata = $this->entityManager->getClassMetadata($fieldMap->reflectionProperty->class);

        /** @var FieldMap[] $fileFields */
        $fileFields = array_reduce($metadata->getFieldMappings(), function (array $acc, FieldMap $fieldMap) {
            if ($fieldMap->type != FileField::class) {
                return $acc;
            }

            $acc[] = $fieldMap;
            return $acc;
        }, []);

        // Собираем id
        $ids = array_reduce($collection, function (array $acc, $el) use ($fileFields) {
            foreach ($fileFields as $field) {
                if ($field->multiple) {
                    if (!empty($el[$field->hydrateName])) {
                        $acc = array_merge($acc, $el[$field->hydrateName]);
                    }
                } else {
                    $acc[] = $el[$field->hydrateName];
                }
            }

            return $acc;
        }, []);

        $ids = array_filter(array_flip(array_flip($ids)));

        if (empty($ids)) {
            self::$cache[$cacheKey] = [];
            return [];
        }

        // Получение данных из базы
        $rs = FileTable::getList([
            'filter' => ['=ID' => $ids]
        ]);
        $result = [];
        while ($el = $rs->fetch()) {
            $result[$el['ID']] = $el;
        }

        // Кешируем для обработки последующих элементов коллекции
        self::$cache[$cacheKey] = $result;

        return $result;
    }

    /**
     * @param null|Context $context
     * @return iterable
     */
    protected function fetchCollection(?Context $context) {
        $collection = null;
        if (
            $context !== null && $context->parent() !== null &&
            (is_array($context->parent()->getInput()) || $context->parent()->getInput() instanceof \Iterator)
        ) {
            $collection = $context->parent()->getInput();

        }

        return $collection;
    }
}