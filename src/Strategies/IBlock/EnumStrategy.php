<?php

namespace Vinds\AnnotationHydratorBitrix\Strategies\IBlock;

use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyWithContainerInterface;
use Vinds\AnnotationHydratorBitrix\IBlock\EnumRepository;
use Vinds\AnnotationHydratorBitrix\Types\Enum\EnumItem;

class EnumStrategy implements StrategyWithContainerInterface {

    /**
     * @var EnumRepository
     */
    protected $enumRepository;

    public function __construct(ContainerInterface $container) {
       $this->enumRepository = $container->get(EnumRepository::class);
    }

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return null|EnumItem
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?EnumItem {
        if ($value === null) {
            return null;
        }

        $iBlockId = $fieldMap->entityMap->options['iBlockId'];

        $enum = $this->enumRepository->get($iBlockId, $fieldMap->name);
        $enumId = $context->getInput()["PROPERTY_{$fieldMap->name}_ENUM_ID"];


        if (!empty($enumId)) {
            return $enum->fetchById($enumId);
        } else {
            return $enum->fetchByValue($value);
        }
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param EnumItem $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return int|null
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context): ?int {
        if ($value === null) {
            return null;
        }

        return $value->getId();
    }
}