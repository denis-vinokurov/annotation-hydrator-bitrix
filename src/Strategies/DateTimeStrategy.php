<?php


namespace Vinds\AnnotationHydratorBitrix\Strategies;


use Bitrix\Main\ObjectException;
use Bitrix\Main\Type\DateTime;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyInterface;

class DateTimeStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return \DateTime|null
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?\DateTime {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof \DateTime) {
            return $value;
        }

        if (is_string($value)) {
            return new \DateTime($value);
        }

        if ($value instanceof DateTime) {
            return (new \DateTime())->setTimestamp($value->getTimestamp());
        }

        return null;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return DateTime|null
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) : ?DateTime {
        if ($value === null) {
            return null;
        }

        try {
            return new DateTime($value->format(DateTime::getFormat()));
        } catch (ObjectException $e) {
            return null;
        }
    }
}