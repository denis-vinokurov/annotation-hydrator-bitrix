<?php


namespace Vinds\AnnotationHydratorBitrix\Strategies;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyInterface;

class BooleanStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return bool
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): bool {
        return $value == 'Y';
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return string
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context): string {
        return $value === true ? 'Y' : 'N';
    }
}