<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;

use Vinds\AnnotationHydrator;
use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\DocBlock\Tag\VarTag;
use Zend\Code\Generator\DocBlockGenerator;
use Zend\Code\Generator\PropertyGenerator;
use Zend\Code\Reflection\MethodReflection;


class ClassBuilder {

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $namespace;

    /**
     * @var array
     */
    private $fields;

    /**
     * @var ClassGenerator
     */
    private $generator;

    /**
     * ClassBuilder constructor.
     * @param array $fields
     * @param null|string $name
     * @param null|string $namespace
     */
    public function __construct(array $fields, ?string $name = null, ?string $namespace = null) {
        $this->fields = $fields;
        $this->name = $name;
        $this->namespace = $namespace;
    }

    /**
     * @return ClassGenerator
     */
    public function getGenerator(): ClassGenerator {
        if ($this->generator === null) {
            $this->generator = new ClassGenerator();
            $this->generator->setDocBlock(new DocBlockGenerator());
        }
        return $this->generator;
    }

    /**
     * @return string
     */
    public function build(): string {
        return '<?php' . ClassGenerator::LINE_FEED . $this->generateClass($this->fields);
    }

    /**
     * @param $fields
     * @return string
     */
    private function generateClass($fields) {
        $class = $this->getGenerator();

        if (!empty($this->namespace)) {
            $class->setNamespaceName($this->namespace);
        }

        $class->addUse(AnnotationHydrator\Annotations\Entity::class);

        foreach ($fields as $field) {
            foreach ($field['annotations'] as $annotation) {
                $class->addUse($annotation);
            }
        }

        $class->setName(!empty($this->name) ? $this->name : 'Entity')
            ->addProperties($this->creteProperties($fields))
            ->getDocBlock()
                ->setTag(['name' => 'Entity()']);

        return $class->generate();
    }

    /**
     * @param $fields
     * @return PropertyGenerator[]
     */
    private function creteProperties($fields) {
        $created = [];

        return array_filter(array_map(function ($field) use (&$created) {
            if ($created[$field['name']] === true) {
                return null;
            }

            $created[$field['name']] = true;

            return (new PropertyGenerator($field['name']))
                ->omitDefaultValue()
                ->setVisibility(PropertyGenerator::VISIBILITY_PROTECTED)
                ->setDocBlock((new DocBlockGenerator())
                    ->setTags(array_map(function ($annotation) use ($field) {
                        $short = explode('\\', $annotation);
                        $short = array_pop($short);

                        if (self::isAnnotationField($annotation)) {
                            $options = array_merge(
                                ['name' =>  $field['fieldName']],
                                is_array($field['options']) ? $field['options'] : []
                            );
                        }

                        if (!empty($options)) {
                            $params = array_map(function ($value, $key) {
                                $value = json_encode($value);
                                return "${key}=${value}";
                            }, $options, array_keys($options));

                            return ['name' => $short . '(' . implode(', ', $params) . ')'];
                        } else {
                            return ['name' => $short . '()'];
                        }
                    }, $field['annotations']))
                    ->setTag($this->createVarTag($field))
                );
        }, $fields));
    }


    /**
     * @param $field
     * @return VarTag
     */
    private function createVarTag($field) {
        $annotation = null;
        $multiple = false;
        foreach ($field['annotations'] as $el) {
            if (self::isAnnotationField($el)) {
                $annotation = $el;
            }

            if ($el === AnnotationHydrator\Annotations\Multiple::class) {
                $multiple = true;
            }
        }

        if ($annotation === null) {
            return new VarTag(null, ['mixed']);
        }

        try {
            $reflectionAnnotation = new \ReflectionClass($annotation);
        } catch (\ReflectionException $e) {
            return new VarTag(null, ['mixed']);
        }

        $strategy = $reflectionAnnotation->getDefaultProperties()['strategy'];

        if (empty($strategy) || !is_subclass_of($strategy, AnnotationHydrator\Strategy\StrategyInterface::class)) {
            return new VarTag(null, ['mixed']);
        }

        try {
            $hydrateMethod = new MethodReflection($strategy, 'hydrate');
        } catch (\ReflectionException $e) {
            return new VarTag(null, ['mixed']);
        }

        $type = $hydrateMethod->getReturnType();


        if (!empty($type)) {
            $types = [$type->getName()];

            if ($type->allowsNull()) {
                $types[] = 'null';
            }
        } else {
            foreach ($hydrateMethod->getDocBlock()->getTags() as $tag) {
                if ($tag instanceof \Zend\Code\Reflection\DocBlock\Tag\ReturnTag) {
                    $types = $tag->getTypes();
                    break;
                }
            }
        }

        if (empty($types)) {
            return new VarTag(null, ['mixed']);
        }

        $types = array_map(function($type) use ($multiple) {
            if (class_exists($type)) {
                $type = '\\' . trim($type, '\\');
            }

            if ($multiple && $type !== 'null') {
                $type .= '[]';
            }

            return $type;
        }, $types);


        return new VarTag(null, $types);
    }

    /**
     * @param $annotation
     * @return bool
     */
    public static function isAnnotationField($annotation) {
        return $annotation === AnnotationHydrator\Annotations\Field::class ||
            is_subclass_of($annotation, AnnotationHydrator\Annotations\Field::class);
    }
}