<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;

use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyTable;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\IBlock;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\Type;
use Vinds\AnnotationHydratorBitrix\AdminSection\Factory\IBlockElementFieldFactory;
use Vinds\AnnotationHydratorBitrix\AdminSection\Factory\UserFieldFactory;

/**
 * Class IBlockFactory
 * @package Vinds\AnnotationHydratorBitrix\AdminSection
 */
class IBlockFactory {

    /**
     * @param $id
     * @return IBlock
     */
    public function __invoke($id) {
        $iBlock = IblockTable::getByPrimary($id)->fetch();

        $result = new IBlock();
        $result->id = (int)$iBlock['ID'];
        $result->name = (string)$iBlock['NAME'];
        $result->code = (string)$iBlock['CODE'];
        $result->element = $this->createElement($id);
        $result->section = $this->createSection($id);

        return $result;
    }

    /**
     * @param $id
     * @return IBlock\Element
     */
    protected function createElement($id) {
        $result = new IBlock\Element();

        $result->types = $this->createElementTypes();

        $result->fields = [
            $this->createElementField(true, 'ID', 'ID элемента', Types::INT, false, true),
            $this->createElementField(false, 'CODE', 'Символьный код', Types::STRING, false, true),
            $this->createElementField(false, 'XML_ID', 'Внешний код', Types::STRING),
            $this->createElementField(false, 'NAME', 'Название элемента', Types::STRING, false, true),
            $this->createElementField(false, 'IBLOCK_ID', 'ID информационного блока', Types::INT, false, true),
            $this->createElementField(false, 'IBLOCK_SECTION_ID', 'ID раздела', Types::REFERENCE, false, false, [
                'repository' => 'iBlockSection' . $id,
                'referenceField' => 'ID',
            ]),
            $this->createElementField(false, 'IBLOCK_CODE', 'Символический код инфоблока', Types::STRING),
            $this->createElementField(false, 'ACTIVE', 'Флаг активности', Types::BOOLEAN),
            $this->createElementField(false, 'ACTIVE_FROM', 'Дата начала действия элемента', Types::DATETIME),
            $this->createElementField(false, 'ACTIVE_TO', 'Дата окончания действия элемента', Types::DATETIME),
            $this->createElementField(false, 'SORT', 'Порядок сортировки элементов', Types::INT),
            $this->createElementField(false, 'PREVIEW_PICTURE', 'Картинка для предварительного просмотра', Types::FILE),
            $this->createElementField(false, 'PREVIEW_TEXT', 'Предварительное описание', Types::STRING),
            $this->createElementField(false, 'PREVIEW_TEXT_TYPE', 'Тип предварительного описания (text/html)', Types::STRING),
            $this->createElementField(false, 'DETAIL_PICTURE', 'Картинка для детального просмотра.', Types::FILE),
            $this->createElementField(false, 'DETAIL_TEXT', 'Детальное описание', Types::STRING),
            $this->createElementField(false, 'DETAIL_TEXT_TYPE', 'Тип детального описания (text/html)', Types::STRING),
            $this->createElementField(false, 'DATE_CREATE', 'Дата создания элемента.', Types::DATETIME),
            $this->createElementField(false, 'CREATED_BY', 'Код пользователя, создавшего элемент', Types::INT),
            $this->createElementField(false, 'CREATED_USER_NAME', 'Имя пользователя, создавшего элемент', Types::STRING),
            $this->createElementField(false, 'TIMESTAMP_X', 'Время последнего изменения полей элемента', Types::DATETIME),
            $this->createElementField(false, 'DETAIL_PAGE_URL', 'Шаблон URL-а к странице для детального просмотра элемента.', Types::STRING),
            $this->createElementField(false, 'MODIFIED_BY', 'Код пользователя, в последний раз изменившего элемент', Types::INT),
            $this->createElementField(false, 'USER_NAME', 'Имя пользователя, в последний раз изменившего элемент', Types::STRING),
            $this->createElementField(false, 'TAGS', 'Теги элемента', Types::STRING),
        ];

        $properties = PropertyTable::getList([
            'filter' => [
                'IBLOCK_ID' => $id
            ]
        ])->fetchAll();

        $result->properties = [];
        $factory = new IBlockElementFieldFactory();
        foreach ($properties as $property) {
            $result->properties[] = $factory($property);
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function createElementTypes() {
        return [
            Type::factory(Types::STRING, 'Строка'),
            Type::factory(Types::INT, 'Целое число'),
            Type::factory(Types::FLOAT, 'Число'),
            Type::factory(Types::FILE, 'Файл'),
            Type::factory(Types::DATETIME, 'Дата/Время'),
            Type::factory(Types::ENUM, 'Список'),
            Type::factory(Types::BOOLEAN, 'Да/Нет'),
            Type::factory(Types::UNDEFINED, 'Не определен'),
            Type::factory(Types::REFERENCE, 'Привязка к элементу'),
        ];
    }

    /**
     * @param bool $primary
     * @param string $code
     * @param string $name
     * @param string $type
     * @param bool $multiple
     * @param bool $defaultSelected
     * @param array $options
     * @return Entity\Field
     */
    protected function createElementField(bool $primary, string $code, string $name, string $type, bool $multiple = false, bool $defaultSelected = false, array $options = []) {

        return Entity\Field::factory(
            $primary,
            $code,
            $name,
            $type,
            $multiple,
            IBlockElementFieldFactory::detectedElementAnnotationField($type),
            $defaultSelected,
            $options
        );
    }

    /**
     * @param $id
     * @return IBlock\Section
     */
    protected function createSection($id) : IBlock\Section {
        $result = new IBlock\Section();

        $result->types = $this->createSectionTypes();

        $result->fields = [
            $this->createSectionField(true, 'ID', 'ID группы информационного блока', Types::INT, false, true),
            $this->createSectionField(false, 'CODE', 'Символьный код', Types::STRING, false, true),
            $this->createSectionField(false, 'XML_ID', 'Внешний код', Types::STRING),
            $this->createSectionField(false, 'IBLOCK_ID', 'ID информационного блока', Types::INT, false, true),

            // Если сделать привязкой к разделу, то при получение раздела может возникнуть рекурсия или как минимум будет выбранно все дерево разделов
            // Так что решено оставить просто целочисленныйм числом
            $this->createSectionField(false, 'IBLOCK_SECTION_ID', 'ID группы родителя, если не задан то группа корневая', Types::INT),

            $this->createSectionField(false, 'TIMESTAMP_X', 'Дата последнего изменения параметров группы', Types::DATETIME),
            $this->createSectionField(false, 'SORT', 'Порядок сортировки (среди групп внутри одной группы-родителя)', Types::INT),
            $this->createSectionField(false, 'NAME', 'Наименование группы', Types::STRING, false, true),
            $this->createSectionField(false, 'ACTIVE', 'Флаг активности', Types::BOOLEAN),
            $this->createSectionField(false, 'GLOBAL_ACTIVE', 'Флаг активности, учитывая активность вышележащих (родительских) групп', Types::BOOLEAN),
            $this->createSectionField(false, 'DESCRIPTION', 'Описание группы', Types::STRING),
            $this->createSectionField(false, 'DESCRIPTION_TYPE', 'Тип описания группы (text/html)', Types::STRING),
            $this->createSectionField(false, 'LEFT_MARGIN', 'Левая граница группы. Вычисляется автоматически', Types::INT),
            $this->createSectionField(false, 'RIGHT_MARGIN', 'Правая граница группы. Вычисляется автоматически', Types::INT),
            $this->createSectionField(false, 'DEPTH_LEVEL', 'Уровень вложенности группы. Вычисляется автоматически', Types::INT),
            $this->createSectionField(false, 'SECTION_PAGE_URL', 'Шаблон URL-а к странице для детального просмотра раздела', Types::STRING),
            $this->createSectionField(false, 'MODIFIED_BY', 'Код пользователя, в последний раз изменившего элемент', Types::INT),
            $this->createSectionField(false, 'DATE_CREATE', 'Дата создания элемента', Types::DATETIME),
            $this->createSectionField(false, 'CREATED_BY', 'Код пользователя, создавшего элемент', Types::INT),
            $this->createSectionField(false, 'PICTURE', 'Картинка', Types::FILE),
            $this->createSectionField(false, 'DETAIL_PICTURE', 'Картинка для детального просмотра', Types::FILE),
        ];

        $rs = \CUserTypeEntity::GetList([], [
            'ENTITY_ID' => "IBLOCK_${id}_SECTION",
            'LANG' => 'ru'
        ]);

        $result->properties = [];
        $factory = new UserFieldFactory();
        while ($property = $rs->Fetch()) {
            $result->properties[] = $factory($property);
        }

        return $result;
    }

    /**
     * @param bool $primary
     * @param string $code
     * @param string $name
     * @param string $type
     * @param bool $multiple
     * @param bool $defaultSelected
     * @param array $options
     * @return Entity\Field
     */
    protected function createSectionField(bool $primary, string $code, string $name, string $type, bool $multiple = false, bool $defaultSelected = false, array $options = []) {

        return Entity\Field::factory(
            $primary,
            $code,
            $name,
            $type,
            $multiple,
            UserFieldFactory::detectedSectionAnnotationField($type),
            $defaultSelected,
            $options
        );
    }

    /**
     * @return array
     */
    protected function createSectionTypes() {
        return [
            Type::factory(Types::STRING, 'Строка'),
            Type::factory(Types::INT, 'Целое число'),
            Type::factory(Types::FLOAT, 'Число'),
            Type::factory(Types::FILE, 'Файл'),
            Type::factory(Types::DATETIME, 'Дата/Время'),
            Type::factory(Types::ENUM, 'Список'),
            Type::factory(Types::BOOLEAN, 'Да/Нет'),
            Type::factory(Types::UNDEFINED, 'Не определен'),
            Type::factory(Types::REFERENCE, 'Привязка к элементу'),
        ];
    }
}