<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Vinds\AnnotationHydrator;
use Vinds\AnnotationHydratorBitrix;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\Type;

class HighloadBlockFactory {

    /**
     * @param $id
     * @return Entity\HighloadBlock
     */
    public function __invoke($id) {
        $result = new Entity\HighloadBlock();

        $result->types = $this->createTypes();

        $filter = ['=ID' => $id];

        if (array_key_exists('LANG', HighloadBlockTable::getMap())) {
            $filter[] = [
                'LOGIC' => 'OR',
                ['=LANG.LID' => Application::getInstance()->getContext()->getLanguage()],
                ['=LANG.LID' => false]
            ];
        }

        $table = HighloadBlockTable::getList([
            'filter' => $filter,
            'select' => ['ID', 'NAME', 'TABLE_NAME','LANG_NAME' => 'LANG.NAME', 'LANG_LID' => 'LANG.LID'],
            'limit' => 1
        ])->fetch();

        $result->id = $id;
        $result->name = !empty($table['LANG_NAME']) ? $table['LANG_NAME'] : $table['NAME'];
        $result->code = $table['TABLE_NAME'];

        $result->fields = [
            $this->createField(true, 'ID', 'ID группы информационного блока', Types::INT, false, true),
        ];

        $rs = \CUserTypeEntity::GetList([], [
            'ENTITY_ID' => "HLBLOCK_${id}",
            'LANG' => 'ru'
        ]);

        while ($field = $rs->Fetch()) {
            $result->fields[] = $this->createField(
                false,
                $field['FIELD_NAME'],
                !empty($field['EDIT_FORM_LABEL']) ? $field['EDIT_FORM_LABEL'] : $field['FIELD_NAME'],
                $this->detectedSectionPropertyType($field),
                $field['MULTIPLE'] == 'Y',
                true
            );
        }

        return $result;
    }


    /**
     * @param bool $primary
     * @param string $code
     * @param string $name
     * @param string $type
     * @param bool $multiple
     * @param bool $defaultSelected
     * @return Entity\Field
     */
    protected function createField(bool $primary, string $code, string $name, string $type, bool $multiple = false, bool $defaultSelected = false) {

        return Entity\Field::factory(
            $primary,
            $code,
            $name,
            $type,
            $multiple,
            $this->detectedSectionAnnotationField($type),
            $defaultSelected
        );
    }

    /**
     * @return array
     */
    protected function createTypes() {
        return [
            Type::factory(Types::STRING, 'Строка'),
            Type::factory(Types::INT, 'Целое число'),
            Type::factory(Types::FLOAT, 'Число'),
            Type::factory(Types::FILE, 'Файл'),
            Type::factory(Types::DATETIME, 'Дата/Время'),
            Type::factory(Types::ENUM, 'Список'),
            Type::factory(Types::BOOLEAN, 'Да/Нет'),
            Type::factory(Types::UNDEFINED, 'Не определен'),
            Type::factory(Types::REFERENCE, 'Привязка к элементу'),
        ];
    }

    /**
     * @param $property
     * @return string
     */
    protected function detectedSectionPropertyType($property) {

        switch ($property['USER_TYPE_ID']) {
            case 'double':
            case 'money':
                $result = Types::FLOAT;
                break;
            case 'integer':
                $result = Types::INT;
                break;
            case 'hlblock':
            case 'iblock_section':
            case 'iblock_element':
                $result = Types::REFERENCE;
                break;
            case 'string':
                $result = Types::STRING;
                break;
            case 'datetime':
            case 'date':
                $result = Types::DATETIME;
                break;
            case 'boolean':
                $result = Types::BOOLEAN;
                break;
            case 'file':
                $result = Types::FILE;
                break;
            case 'enumeration':
                $result = Types::ENUM;
                break;

            default:
                $result = Types::UNDEFINED;
        }

        return $result;
    }

    /**
     * @param $type
     * @return string
     */
    protected function detectedSectionAnnotationField($type) {
        switch ($type) {
            case Types::STRING:
                $result = AnnotationHydrator\Annotations\StringField::class;
                break;
            case Types::INT:
                $result = AnnotationHydrator\Annotations\IntField::class;
                break;
            case Types::FLOAT:
                $result = AnnotationHydrator\Annotations\FloatField::class;
                break;
            case Types::FILE:
                $result = AnnotationHydratorBitrix\Annotations\FileField::class;
                break;
            case Types::DATETIME:
                $result = AnnotationHydratorBitrix\Annotations\DateTimeField::class;
                break;
            case Types::BOOLEAN:
                $result = AnnotationHydratorBitrix\Annotations\BooleanField::class;
                break;
            case Types::ENUM:
                $result = AnnotationHydratorBitrix\Annotations\UserField\EnumField::class;
                break;
            case Types::REFERENCE:
                $result = AnnotationHydrator\Annotations\ReferenceField::class;
                break;
            case Types::UNDEFINED:
            default:
                $result = AnnotationHydrator\Annotations\Field::class;
                break;
        }

        return $result;
    }
}