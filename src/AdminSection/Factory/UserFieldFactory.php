<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection\Factory;


use Vinds\AnnotationHydratorBitrix\AdminSection\Types;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity;
use Vinds\AnnotationHydrator;
use Vinds\AnnotationHydratorBitrix;

/**
 * Class UserFieldFactory
 * @package Vinds\AnnotationHydratorBitrix\AdminSection\Factory
 */
class UserFieldFactory {
    /**
     * @param $field
     * @return Entity\Field
     */
    public function __invoke($field) {
        $type = $this->detectedSectionPropertyType($field);

        return Entity\Field::factory(
            false,
            $field['FIELD_NAME'],
            !empty($field['EDIT_FORM_LABEL']) ? $field['EDIT_FORM_LABEL'] : $field['FIELD_NAME'],
            $type,
            $field['MULTIPLE'] == 'Y',
            self::detectedSectionAnnotationField($type),
            true,
            $this->createOptions($field)
        );
    }


    /**
     * @param $field
     * @return string
     */
    protected function detectedSectionPropertyType($field) {

        switch ($field['USER_TYPE_ID']) {
            case 'double':
            case 'money':
                $result = Types::FLOAT;
                break;
            case 'integer':
                $result = Types::INT;
                break;
            case 'hlblock':
            case 'iblock_section':
            case 'iblock_element':
                $result = Types::REFERENCE;
                break;
            case 'string':
                $result = Types::STRING;
                break;
            case 'datetime':
            case 'date':
                $result = Types::DATETIME;
                break;
            case 'boolean':
                $result = Types::BOOLEAN;
                break;
            case 'file':
                $result = Types::FILE;
                break;
            case 'enumeration':
                $result = Types::ENUM;
                break;

            default:
                $result = Types::UNDEFINED;
        }

        return $result;
    }

    /**
     * @param $field
     * @return array
     */
    protected function createOptions($field) {
        $options = [];
        $type = $this->detectedSectionPropertyType($field);
        if ($type === Types::REFERENCE) {
            switch ($field['USER_TYPE_ID']) {
                case 'iblock_element':
                    $repository = 'iBlockElement' . $field['SETTINGS']['IBLOCK_ID'];
                    $referenceField = 'ID';
                    break;
                case 'iblock_section':
                    $repository = 'iBlockSection' . $field['SETTINGS']['IBLOCK_ID'];
                    $referenceField = 'ID';
                    break;
                case 'hlblock':
                    $repository = 'HighLoadBlock' . $field['SETTINGS']['HLBLOCK_ID'];;
                    $referenceField = 'ID';
                    break;
            }

            if (!empty($repository) && !empty($referenceField)) {
                $options['repository'] = $repository;
                $options['referenceField'] = $referenceField;
            }
        }

        return $options;
    }



    /**
     * @param $type
     * @return string
     */
    public static function detectedSectionAnnotationField($type) {
        switch ($type) {
            case Types::STRING:
                $result = AnnotationHydrator\Annotations\StringField::class;
                break;
            case Types::INT:
                $result = AnnotationHydrator\Annotations\IntField::class;
                break;
            case Types::FLOAT:
                $result = AnnotationHydrator\Annotations\FloatField::class;
                break;
            case Types::FILE:
                $result = AnnotationHydratorBitrix\Annotations\FileField::class;
                break;
            case Types::DATETIME:
                $result = AnnotationHydratorBitrix\Annotations\DateTimeField::class;
                break;
            case Types::BOOLEAN:
                $result = AnnotationHydratorBitrix\Annotations\BooleanField::class;
                break;
            case Types::ENUM:
                $result = AnnotationHydratorBitrix\Annotations\UserField\EnumField::class;
                break;
            case Types::REFERENCE:
                $result = AnnotationHydrator\Annotations\ReferenceField::class;
                break;
            case Types::UNDEFINED:
            default:
                $result = AnnotationHydrator\Annotations\Field::class;
                break;
        }

        return $result;
    }
}