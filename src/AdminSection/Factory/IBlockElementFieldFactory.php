<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection\Factory;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Iblock\PropertyTable;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\Field;
use Vinds\AnnotationHydratorBitrix\AdminSection\Types;
use Vinds\AnnotationHydrator;
use Vinds\AnnotationHydratorBitrix;

/**
 * Class IBlockElementFieldFactory
 * @package Vinds\AnnotationHydratorBitrix\AdminSection\Factory
 */
class IBlockElementFieldFactory {

    /**
     * @param $field
     * @return Field
     */
    public function __invoke($field) {
        $type = $this->detectedElementPropertyType($field);

        return Field::factory(
            false,
            $field['CODE'],
            $field['NAME'],
            $type,
            $field['MULTIPLE'] == 'Y',
            self::detectedElementAnnotationField($type),
            true,
            $this->createOptions($field)
        );
    }



    /**
     * @param $property
     * @return string
     */
    protected function detectedElementPropertyType($property) {
        switch ($property['PROPERTY_TYPE']) {
            case PropertyTable::TYPE_STRING:
                if ($property['USER_TYPE'] === 'directory') {
                    $result = Types::REFERENCE;
                } else {
                    $result = Types::STRING;
                }
                break;
            case PropertyTable::TYPE_NUMBER:
                $result = Types::FLOAT;
                break;
            case PropertyTable::TYPE_FILE:
                $result = Types::FILE;
                break;
            case PropertyTable::TYPE_ELEMENT:
                $result = Types::REFERENCE;
                break;
            case PropertyTable::TYPE_SECTION:
                $result = Types::REFERENCE;
                break;
            case PropertyTable::TYPE_LIST:
                $result = Types::ENUM;
                break;
            default:
                $result = Types::UNDEFINED;
        }

        return $result;
    }

    /**
     * @param $field
     * @return array
     */
    protected function createOptions($field) {
        $type = $this->detectedElementPropertyType($field);
        $options = [];
        if ($type === Types::REFERENCE) {
            $repository = null;
            $referenceField = null;

            if ($field['PROPERTY_TYPE'] === PropertyTable::TYPE_ELEMENT) {
                $repository = 'iBlockElement' . $field['LINK_IBLOCK_ID'];
                $referenceField = 'ID';
            } elseif ($field['PROPERTY_TYPE'] === PropertyTable::TYPE_SECTION) {
                $repository = 'iBlockSection' . $field['LINK_IBLOCK_ID'];
                $referenceField = 'ID';
            } elseif ($field['USER_TYPE'] === 'directory') {
                $settings = unserialize($field['USER_TYPE_SETTINGS']);

                if (!empty($settings) && !empty($settings['TABLE_NAME'])) {
                    $highLoad = HighloadBlockTable::getList([
                        'filter' => [
                            '=TABLE_NAME' => $settings['TABLE_NAME']
                        ]
                    ])->fetch();

                    if (!empty($highLoad)) {
                        $repository = 'HighLoadBlock' . $highLoad['ID'];
                        $referenceField = 'UF_XML_ID';
                    }
                }
            }

            if (!empty($repository) && !empty($referenceField)) {
                $options['repository'] = $repository;
                $options['referenceField'] = $referenceField;
            }
        }

        return $options;
    }

    /**
     * @param $type
     * @return string
     */
    public static function detectedElementAnnotationField($type) {
        switch ($type) {
            case Types::STRING:
                $result = AnnotationHydrator\Annotations\StringField::class;
                break;
            case Types::INT:
                $result = AnnotationHydrator\Annotations\IntField::class;
                break;
            case Types::FLOAT:
                $result = AnnotationHydrator\Annotations\FloatField::class;
                break;
            case Types::FILE:
                $result = AnnotationHydratorBitrix\Annotations\FileField::class;
                break;
            case Types::DATETIME:
                $result = AnnotationHydratorBitrix\Annotations\DateTimeField::class;
                break;
            case Types::ENUM:
                $result = AnnotationHydratorBitrix\Annotations\IBlock\EnumField::class;
                break;
            case Types::REFERENCE:
                $result = AnnotationHydrator\Annotations\ReferenceField::class;
                break;
            case Types::BOOLEAN:
            case Types::UNDEFINED:
            default:
                $result = AnnotationHydrator\Annotations\Field::class;
                break;
        }

        return $result;
    }
}