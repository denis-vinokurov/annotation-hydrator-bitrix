<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;

use Vinds\AnnotationHydrator;
use Vinds\AnnotationHydratorBitrix;
use Vinds\AnnotationHydratorBitrix\AdminSection\Factory\UserFieldFactory;
use Zend\Filter\FilterChain;
use Zend\Filter\StringToLower;
use Zend\Filter\Word\UnderscoreToCamelCase;

class HigloadBlockBuilder {

    /**
     * @var Entity\HighloadBlock
     */
    protected $entity;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     * @return string
     */
    public static function build(array $data) {
        $instance = new static();

        $factory = new HighloadBlockFactory();

        $instance->entity = $factory($data['id']);
        $instance->data   = $data;

        $result = null;

        $result = $instance->buildElement();

        return $result;
    }

    /**
     * @return string
     */
    private function buildElement() {
        $nameFilter = (new FilterChain())
            ->attach(function ($value) {
                return preg_replace('#^UF_#' , '', $value);
            })
            ->attach(new StringToLower())
            ->attach(new UnderscoreToCamelCase())
            ->attach('lcfirst');

        $fields = array_map(function (Entity\Field $el) use ($nameFilter) {
            if (!array_key_exists($el->code, $this->data['fields'])) {
                return null;
            }

            $data = $this->data['fields'][$el->code];

            $result = [
                'name'        => $nameFilter->filter($el->code),
                'fieldName'   => $el->code,
                'annotations' => [],
            ];

            if (array_key_exists($el->code, $this->data['fields'])) {
                if ($el->primary) {
                    $result['annotations'][] = AnnotationHydrator\Annotations\Primary::class;
                }
            } elseif (array_key_exists($el->code, $this->data['properties'])) {
                $result['annotations'][] = AnnotationHydratorBitrix\Annotations\IBlock\Property::class;
            }


            if ($el->multiple) {
                $result['annotations'][] = AnnotationHydrator\Annotations\Multiple::class;
            }

            $result['annotations'][] = UserFieldFactory::detectedSectionAnnotationField($data['type']);
            $result['options'] = $data['options'];

            return $result;
        }, $this->entity->fields);
        $fields = array_filter($fields);


        $classBuilder = new ClassBuilder($fields, $this->data['name'], $this->data['namespace']);

        $classBuilder->getGenerator()
            ->addUse(AnnotationHydratorBitrix\Annotations\HighloadBlock\HLBlockId::class)
            ->getDocBlock()
                ->setTag(['name' => 'HLBlockId(' . $this->entity->code . ')']);

        return $classBuilder->build();
    }

}