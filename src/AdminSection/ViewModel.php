<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

class ViewModel {

    protected $data;

    public function __construct() {

        $this->loadIBlockData();
        $this->loadHighloadBlockData();
    }

    public function __toString() {
        return json_encode($this->data);
    }


    private function loadIBlockData() {
        try {
            Loader::includeModule('iblock');
        } catch (LoaderException $e) {
            $this->data['iBlocks'] = [];
            return;
        }

        $rs = IblockTable::getList([
            'select' => ['ID']
        ]);

        $factory = new IBlockFactory();
        $iBlocks = [];
        while ($el = $rs->fetch()) {
            $iBlocks[] = $factory($el['ID']);
        }

        $this->data['iBlocks'] = $iBlocks;
    }

    private function loadHighloadBlockData() {
        try {
            Loader::includeModule('highloadblock');
        } catch (LoaderException $e) {
            $this->data['highLoadBlocks'] = [];
            return;
        }
        $rs = HighloadBlockTable::getList([
            'select' => ['ID']
        ]);

        $factory = new HighloadBlockFactory();
        $items = [];
        while ($el = $rs->fetch()) {
            $items[] = $factory($el['ID']);
        }

        $this->data['HLBlocks'] = $items;
    }
}