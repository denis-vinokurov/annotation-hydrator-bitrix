<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection;


use Vinds\AnnotationHydrator;
use Vinds\AnnotationHydratorBitrix;
use Vinds\AnnotationHydratorBitrix\AdminSection\Factory\IBlockElementFieldFactory;
use Vinds\AnnotationHydratorBitrix\AdminSection\Factory\UserFieldFactory;
use Zend\Filter\FilterChain;
use Zend\Filter\StringToLower;
use Zend\Filter\Word\UnderscoreToCamelCase;

class IBlockBuilder {

    /**
     * @var Entity\IBlock
     */
    protected $entity;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     * @return string
     */
    public static function build(array $data) {
        $instance = new static();

        $factory = new IBlockFactory();

        $instance->entity = $factory($data['id']);
        $instance->data   = $data;

        $result = null;
        if ($data['entityType'] == 'element') {
            $result = $instance->buildElement();
        } elseif ($data['entityType'] == 'section') {
            $result = $instance->buildSection();
        }

        return $result;
    }

    /**
     * @return string
     */
    private function buildElement() {
        $nameFilter = (new FilterChain())
            ->attach(new StringToLower())
            ->attach(new UnderscoreToCamelCase())
            ->attach('lcfirst');

        $fields = array_map(function (AnnotationHydratorBitrix\AdminSection\Entity\Field $el) use ($nameFilter) {
            if (array_key_exists($el->code, $this->data['fields']) && array_key_exists($el->code, $this->data['properties'])) {
                throw new AnnotationHydratorBitrix\Exception(
                    "Поля и свойство имеют одинаковый сивольный код.\r\nСимвольный код должен быть уникален. CODE: " . $el->code
                );
            }

            if (!array_key_exists($el->code, $this->data['fields']) && !array_key_exists($el->code, $this->data['properties'])) {
                return null;
            }

            $data = array_key_exists($el->code, $this->data['fields'])
                ? $this->data['fields'][$el->code]
                : $this->data['properties'][$el->code];

            $result = [
                'name'        => $nameFilter->filter($el->code),
                'fieldName'   => $el->code,
                'annotations' => [],
            ];

            if (array_key_exists($el->code, $this->data['fields'])) {
                if ($el->primary) {
                    $result['annotations'][] = AnnotationHydrator\Annotations\Primary::class;
                }
            } elseif (array_key_exists($el->code, $this->data['properties'])) {
                $result['annotations'][] = AnnotationHydratorBitrix\Annotations\IBlock\Property::class;
            }


            if ($el->multiple) {
                $result['annotations'][] = AnnotationHydrator\Annotations\Multiple::class;
            }

            $result['annotations'][] = IBlockElementFieldFactory::detectedElementAnnotationField($data['type']);
            $result['options'] = $data['options'];

            return $result;
        }, array_merge($this->entity->element->fields, $this->entity->element->properties));
        $fields = array_filter($fields);


        $classBuilder = new ClassBuilder($fields, $this->data['name'], $this->data['namespace']);

        $classBuilder->getGenerator()
            ->addUse(AnnotationHydratorBitrix\Annotations\IBlock\IBlockId::class)
            ->getDocBlock()
                ->setTag(['name' => 'IBlockId(' . $this->entity->code . ')']);

        return $classBuilder->build();
    }

    private function buildSection() {
        $nameFilter = (new FilterChain())
            ->attach(function ($value) {
                return preg_replace('#^UF_#' , '', $value);
            })
            ->attach(new StringToLower())
            ->attach(new UnderscoreToCamelCase())
            ->attach('lcfirst');


        $fields = array_map(function (AnnotationHydratorBitrix\AdminSection\Entity\Field $el) use ($nameFilter) {
            if (array_key_exists($el->code, $this->data['fields']) && array_key_exists($el->code, $this->data['properties'])) {
                throw new AnnotationHydratorBitrix\Exception(
                    "Поля и свойство имеют одинаковый сивольный код.\r\nСимвольный код должен быть уникален. CODE: " . $el->code
                );
            }

            if (!array_key_exists($el->code, $this->data['fields']) && !array_key_exists($el->code, $this->data['properties'])) {
                return null;
            }

            $data = array_key_exists($el->code, $this->data['fields'])
                ? $this->data['fields'][$el->code]
                : $this->data['properties'][$el->code];

            $result = [
                'name'        => $nameFilter->filter($el->code),
                'fieldName'   => $el->code,
                'annotations' => [],
            ];

            if (array_key_exists($el->code, $this->data['fields'])) {
                if ($el->primary) {
                    $result['annotations'][] = AnnotationHydrator\Annotations\Primary::class;
                }
            }


            if ($el->multiple) {
                $result['annotations'][] = AnnotationHydrator\Annotations\Multiple::class;
            }

            $result['annotations'][] = UserFieldFactory::detectedSectionAnnotationField($data['type']);
            $result['options'] = $data['options'];

            return $result;
        }, array_merge($this->entity->section->fields, $this->entity->section->properties));
        $fields = array_filter($fields);

        $classBuilder = new ClassBuilder($fields, $this->data['name'], $this->data['namespace']);

        $classBuilder->getGenerator()
            ->addUse(AnnotationHydratorBitrix\Annotations\IBlock\IBlockId::class)
            ->getDocBlock()
                ->setTag(['name' => 'IBlockId(' . $this->entity->code . ')']);

        return $classBuilder->build();
    }

}