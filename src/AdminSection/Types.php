<?php

namespace Vinds\AnnotationHydratorBitrix\AdminSection;

class Types {
    const STRING    = 'string';
    const INT       = 'int';
    const FLOAT     = 'float';
    const FILE      = 'file';
    const DATETIME  = 'datetime';
    const ENUM      = 'enum';
    const BOOLEAN   = 'boolean';
    const REFERENCE = 'reference';

    const UNDEFINED = 'undefined';
}