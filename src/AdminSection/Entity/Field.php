<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection\Entity;

/**
 * Class Field
 * @package Vinds\AnnotationHydratorBitrix\AdminSection\Entity\IBlock
 */
class Field {
    /**
     * @var bool
     */
    public $primary;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;

    /**
     * @var Type
     */
    public $type;

    /**
     * Выбранно по умолчанию?
     * @var bool
     */
    public $defaultSelected;

    /**
     * @var bool
     */
    public $multiple;

    /**
     * @var string
     */
    public  $annotation;

    /**
     * @var array
     */
    public $options;

    public static function factory(
        bool $primary,
        string $code,
        string $name,
        string $type,
        bool $multiple = false,
        string $annotation = \Vinds\AnnotationHydrator\Annotations\Field::class,
        bool $defaultSelected = false,
        array $options = []
        )
    {
        $instance = new static();

        $instance->primary = $primary;
        $instance->code = $code;
        $instance->name = $name;
        $instance->type = $type;
        $instance->multiple = $multiple;
        $instance->annotation = $annotation;
        $instance->defaultSelected = $defaultSelected;
        $instance->options = $options;

        return $instance;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return [
            'primary'         => (bool)$this->primary,
            'name'            => (string)$this->name,
            'code'            => (string)$this->code,
            'type'            => $this->type,
            'defaultSelected' => (bool)$this->defaultSelected,
            'options'         => $this->options,
        ];
    }
}