<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection\Entity;


class HighloadBlock implements \JsonSerializable {

    /**
     * @var number
     */
    public $id;


    /**
     * @var string
     */
    public $name;


    /**
     * @var string
     */
    public $code;

    /**
     * @var Type
     */
    public $types;

    /**
     * @var Field[]
     */
    public $fields;

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return [
            'id'     => (int)$this->id,
            'name'   => (string)$this->name,
            'code'   => (string)$this->code,
            'types'  => $this->types,
            'fields' => $this->fields,
        ];
    }
}
