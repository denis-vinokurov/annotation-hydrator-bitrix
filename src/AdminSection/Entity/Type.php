<?php


namespace Vinds\AnnotationHydratorBitrix\AdminSection\Entity;


class Type implements \JsonSerializable {

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;

    public static function factory(string $code, string $name) {
        $type = new static();
        $type->code = $code;
        $type->name = $name;
        return $type;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'code' => $this->code,
        ];
    }
}