<?php

namespace Vinds\AnnotationHydratorBitrix\AdminSection\Entity\IBlock;

use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\Field;
use Vinds\AnnotationHydratorBitrix\AdminSection\Entity\Type;


/**
 * Class Element
 * @package Vinds\AnnotationHydratorBitrix\AdminSection\Entity\IBlock
 */
class Element implements \JsonSerializable {

    /**
     * @var Type
     */
    public $types;

    /**
     * @var Field[]
     */
    public $fields;

    /**
     * @var Field[]
     */
    public $properties;


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return [
            'types' => $this->types,
            'fields' => $this->fields,
            'properties' => $this->properties,
        ];
    }
}