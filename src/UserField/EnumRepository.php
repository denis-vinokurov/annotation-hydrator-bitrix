<?php


namespace Vinds\AnnotationHydratorBitrix\UserField;

use Bitrix\Main\UserFieldTable;
use Vinds\AnnotationHydratorBitrix\Types\Enum;
use Vinds\AnnotationHydratorBitrix\Types\Enum\EnumFactory;

class EnumRepository {

    /**
     * @var Enum[]
     */
    protected $map = [];

    /**
     * @var EnumFactory
     */
    protected $enumFactory;

    /**
     * @var \CUserFieldEnum
     */
    protected $cUserFieldEnum;

    /**
     * @param EnumFactory $enumFactory
     */
    public function __construct(EnumFactory $enumFactory) {
        $this->enumFactory = $enumFactory;

        $this->cUserFieldEnum = new \CUserFieldEnum();
    }

    /**
     * @param string $entityId
     * @param string $code
     * @return Enum
     */
    public function get(string $entityId, string $code): Enum {
        $key = "USER-FIELD-${entityId}CODE-${code}";

        if (array_key_exists($key, $this->map)) {
            return $this->map[$key];
        }

        $enum = $this->load($entityId, $code);
        $this->map[$key] = $enum;

        return $enum;
    }

    /**
     * @param $entityId
     * @param $code
     * @param $key
     * @return Enum
     */
    protected function load($entityId, $code): Enum {
        $field = UserFieldTable::getList([
            'select' => ['ID'],
            'filter' => [
                '=ENTITY_ID' => $entityId,
                'FIELD_NAME' => $code,
            ]
        ])->fetch();

        if (empty($field)) {
            return null;
        }

        $rs = $this->cUserFieldEnum->GetList(['SORT' => 'ASC'], [
            'USER_FIELD_ID' => $field['ID'],
        ]);

        $data = [];

        while ($el = $rs->Fetch()) {
            $data[] = $el;
        }

        return self::createEnum($data);
    }

    /**
     * @param array $data
     * @return Enum
     */
    protected function createEnum(array $data): Enum {
        $factory = $this->enumFactory;
        return $factory($data);
    }


}