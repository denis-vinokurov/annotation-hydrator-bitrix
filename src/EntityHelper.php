<?php


namespace Vinds\AnnotationHydratorBitrix;


use Vinds\AnnotationHydrator\Annotations\Postfix;
use Vinds\AnnotationHydrator\Annotations\Prefix;
use Vinds\AnnotationHydrator\EntityManager;

class EntityHelper {
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * EntityHelper constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string|object $entity
     * @param bool $prefix
     * @param bool $postfix
     * @return array
     */
    public function getFields($entity, $prefix = true, $postfix = true) {
        if (is_object($entity)) {
            $entity = get_class($entity);
        }

        $metadata = $this->entityManager->getClassMetadata($entity);

        $fields = [];
        foreach ($metadata->getFieldMappings() as $fieldMap) {
            $fieldName = $fieldMap->name;

            if ($prefix) {
                foreach ($fieldMap->options as $option) {
                    if ($option instanceof Prefix) {
                        $fieldName = $option->hydrate . $fieldName;
                        break;
                    }
                }
            }

            if ($postfix) {
                foreach ($fieldMap->options as $option) {
                    if ($option instanceof Postfix) {
                        $fieldName = $fieldName . $option->hydrate;
                        break;
                    }
                }
            }

            $fields[] = $fieldName;
        }

        return $fields;
    }
}