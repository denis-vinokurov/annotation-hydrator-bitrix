<?php


namespace Vinds\AnnotationHydratorBitrix\IBlock;

use Vinds\AnnotationHydratorBitrix\BitrixCache;
use Vinds\AnnotationHydratorBitrix\Types\Enum;
use Vinds\AnnotationHydratorBitrix\Types\Enum\EnumFactory;

class EnumRepository {

    /**
     * @var Enum[]
     */
    protected $map = [];

    /**
     * @var BitrixCache
     */
    protected $cache;

    /**
     * @var EnumFactory
     */
    protected $enumFactory;

    /**
     * @param EnumFactory $enumFactory
     * @param BitrixCache $cache
     */
    public function __construct(EnumFactory $enumFactory, BitrixCache $cache) {
        $this->enumFactory = $enumFactory;
        $this->cache = $cache;
    }

    /**
     * @param $iBlockId
     * @param $code
     * @return Enum
     */
    public function get($iBlockId, $code): Enum {
        $key = "IB-${iBlockId}CODE-${code}";

        if (array_key_exists($key, $this->map)) {
            return $this->map[$key];
        }

        $enum = $this->load($iBlockId, $code, $key);
        $this->map[$key] = $enum;

        return $enum;
    }

    /**
     * @param $iBlockId
     * @param $code
     * @param $key
     * @return Enum
     */
    protected function load($iBlockId, $code, $key): Enum {
        if (false === ($data = $this->cache->fetch($key))) {
            $rs = \CIBlockPropertyEnum::GetList(['SORT' => 'ASC'], [
                'IBLOCK_ID' => $iBlockId,
                'CODE'      => $code
            ]);

            $data = [];

            while ($el = $rs->Fetch()) {
                $data[] = $el;
            }

            $this->cache->tag("iblock_id_${iBlockId}");
            $this->cache->save($key, $data);
        }

        return self::createEnum($data);
    }

    /**
     * @param array $data
     * @return Enum
     */
    protected function createEnum(array $data): Enum {
        $factory = $this->enumFactory;
        return $factory($data);
    }


}