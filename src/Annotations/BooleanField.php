<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations;

use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Field;
use Vinds\AnnotationHydratorBitrix\Strategies\BooleanStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class BooleanField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class BooleanField extends Field {

    /**
     * @var string
     */
    public $strategy = BooleanStrategy::class;
}