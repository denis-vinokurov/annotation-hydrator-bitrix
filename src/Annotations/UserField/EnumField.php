<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations\UserField;

use Vinds\AnnotationHydrator\Annotations\Field;
use Vinds\AnnotationHydratorBitrix\Strategies\UserField\EnumStrategy;

/**
 * @Annotation()
 * @Target({"PROPERTY"})
 * Class EnumField
 * @package Vinds\AnnotationHydratorBitrix\Annotations\UserField
 */
class EnumField extends Field {

    /**
     * @var string
     */
    public $strategy = EnumStrategy::class;
}