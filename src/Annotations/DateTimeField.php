<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations;

use Bitrix\Main\ObjectException;
use Bitrix\Main\Type\DateTime;
use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydratorBitrix\Strategies\DateTimeStrategy;


/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class DateTimeField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class DateTimeField extends \Vinds\AnnotationHydrator\Annotations\DateTimeField {

    /**
     * @var string
     */
    public $strategy = DateTimeStrategy::class;
}