<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations\HighloadBlock;

use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Options;
use Vinds\AnnotationHydrator\Exception\InvalidArgumentException;
use Vinds\AnnotationHydrator\Exception\UnexpectedValueException;


/**
 * @Annotation()
 * @Target({"CLASS"})
 * Class HLBlockId
 * @package Vinds\AnnotationHydratorBitrix\Annotations\HighloadBlock
 */
class HLBlockId extends Options {
    /**
     * IBlockId constructor.
     * @param $value
     */
    public function __construct($value) {
        if (!isset($value['value'])) {
            throw new InvalidArgumentException('Не указан id инфоблока');
        }

        if (!is_numeric($value['value'])) {
            throw new UnexpectedValueException("id инфоблока должно быть числом. текущее значение ${value['value']}");
        }

        $value = (int) $value['value'];
        $this->value = [
            'hlBlockId' => $value,
            'userFieldEntity' => 'HLBLOCK_' . $value,
        ];
    }
}