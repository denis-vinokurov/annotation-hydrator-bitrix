<?php


namespace Vinds\AnnotationHydratorBitrix\Annotations\IBlock;
use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Field;
use Vinds\AnnotationHydratorBitrix\Strategies\IBlock\EnumStrategy;

/**
 * @Annotation()
 * @Target({"PROPERTY"})
 * Class Enum
 * @package Vinds\AnnotationHydratorBitrix\Annotations\IBlock
 */
class EnumField extends Field {

    /**
     * @var string
     */
    public $strategy = EnumStrategy::class;
}