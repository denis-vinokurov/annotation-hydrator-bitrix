<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations\IBlock;

use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Options;
use Vinds\AnnotationHydrator\Annotations\Postfix;
use Vinds\AnnotationHydrator\Annotations\Prefix;

/**
 * @Annotation()
 * @Target({"PROPERTY"})
 * Class Property
 * @package Vinds\AnnotationHydratorBitrix\Annotations\IBlock
 */
final class Property extends Options {

    /**
     * @var Prefix
     */
    protected static $prefix;

    /**
     * @var Postfix
     */
    protected static $postfix;

    /**
     * Property constructor.
     */
    public function __construct() {
        $this->value = [
            clone self::getPrefix(),
            clone self::getPostfix(),
        ];
    }

    /**
     * @return Prefix
     */
    protected static function getPrefix() {
        if (self::$prefix !== null) {
            return self::$prefix;
        }

        $prefix = new Prefix();
        $prefix->hydrate = 'PROPERTY_';
        $prefix->extract = 'PROPERTY_';

        self::$prefix = $prefix;

        return $prefix;
    }

    /**
     * @return Postfix
     */
    protected static function getPostfix() {
        if (self::$postfix !== null) {
            return self::$postfix;
        }

        $postfix = new Postfix();
        $postfix->hydrate = '_VALUE';
        $postfix->extract = '_VALUE';

        self::$postfix = $postfix;

        return $postfix;
    }
}