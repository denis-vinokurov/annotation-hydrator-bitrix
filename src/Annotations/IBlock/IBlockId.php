<?php


namespace Vinds\AnnotationHydratorBitrix\Annotations\IBlock;
use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Options;
use Vinds\AnnotationHydrator\Exception\InvalidArgumentException;
use Vinds\AnnotationHydrator\Exception\UnexpectedValueException;


/**
 * @Annotation()
 * @Target({"CLASS"})
 * Class IBlockCode
 * @package Vinds\AnnotationHydratorBitrix\Annotations\IBlock
 */
class IBlockId extends Options {
    /**
     * IBlockId constructor.
     * @param $value
     */
    public function __construct($value) {
        if (!isset($value['value'])) {
            throw new InvalidArgumentException('Не указан id инфоблока');
        }

        if (!is_numeric($value['value'])) {
            throw new UnexpectedValueException("id инфоблока должно быть числом. текущее значение ${value['value']}");
        }

        $this->value = [
            'iBlockId' => (int) $value['value'],
            'userFieldEntity' => 'IBLOCK_' . $value . '_SECTION',
        ];
    }
}