<?php

namespace Vinds\AnnotationHydratorBitrix\Annotations;
use Bitrix\Main\FileTable;
use Vinds\AnnotationHydrator\Annotations\Field;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydratorBitrix\Strategies\FileStrategy;
use Vinds\AnnotationHydratorBitrix\Types\File;

/**
 * @Annotation()
 * Class FileField
 * @package Vinds\AnnotationHydratorBitrix\Annotations
 */
class FileField extends Field {

    /**
     * @var string
     */
    public $strategy = FileStrategy::class;

}