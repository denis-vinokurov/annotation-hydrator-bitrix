import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppComponent} from './app.component';
import {MatCardModule, MatExpansionModule, MatProgressBarModule} from "@angular/material";
import {InfoBlockModule} from "./info-block/info-block.module";
import {ListsComponent} from './components/lists/lists.component';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {DataService} from "./shared/services/data.service";
import {HttpClientModule} from "@angular/common/http";
import {HLBlockModule} from "./hl-block/hl-block.module";
import {RoutingModule} from "./routing/routing.module";
import {SharedModule} from "./shared/shared.module";

@NgModule({
    declarations: [
        AppComponent,
        ListsComponent
    ],
    imports: [
        BrowserModule, BrowserAnimationsModule, HttpClientModule,

        InfoBlockModule, HLBlockModule, SharedModule, RoutingModule,

        MatExpansionModule, MatCardModule, MatProgressBarModule,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        DataService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
