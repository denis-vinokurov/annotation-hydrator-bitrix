import {SectionInterface} from "./section";
import {ElementInterface} from "./element";

export interface InfoBlockInterface {
    id: number;
    name: string;
    code: string;

    section: SectionInterface;
    element: ElementInterface;
}
