import {inject, TestBed} from '@angular/core/testing';
import {InfoBlockService} from './info-block.service';

describe('InfoBlockService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [InfoBlockService]
        });
    });

    it('should be created', inject([InfoBlockService], (service: InfoBlockService) => {
        expect(service).toBeTruthy();
    }));
});
