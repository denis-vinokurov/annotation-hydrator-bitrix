import {TypeInterface} from "../../shared/services/type-interface";
import {FieldInterface} from "../../shared/services/field-interface";

export interface SectionInterface {
    types: Array<TypeInterface>

    fields: Array<FieldInterface>,
    properties: Array<FieldInterface>,
}
