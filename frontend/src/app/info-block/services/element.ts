import {TypeInterface} from "../../shared/services/type-interface";
import {FieldInterface} from "../../shared/services/field-interface";


export interface ElementInterface {
    types: Array<TypeInterface>

    fields: Array<FieldInterface>,
    properties: Array<FieldInterface>,
}