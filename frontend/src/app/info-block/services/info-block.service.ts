import {Injectable} from '@angular/core';
import {InfoBlockInterface} from "./info-block";
import {DataService} from "../../shared/services/data.service";
import {map} from "rxjs/operators";
import {Observable} from "rxjs/internal/Observable";
import {HttpClient} from "@angular/common/http";
import {HttpHelperService} from "../../shared/services/http-helper.service";


@Injectable()
export class InfoBlockService {
    public constructor(private dataService: DataService, private httpClient: HttpClient) {
    }

    public getList(): Observable<Array<InfoBlockInterface>> {
        return this.dataService.fetch()
            .pipe(
                map(data => data.iBlocks)
            );
    }

    getById(id: number): Observable<InfoBlockInterface> {
        return this.getList()
            .pipe(
                map((iBlocks: Array<InfoBlockInterface>) => {
                    return iBlocks.find((el) => el.id === id);
                })
            );
    }

    /**
     * @param {object} value
     * @returns {Observable<Object>}
     */
    createEntity(value: object) {
        return this.httpClient.post('/bitrix/admin/annotation-hydrator.php?type=iBlock', HttpHelperService.serialize(value), {
            withCredentials: true,
            responseType: 'text',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        });
    }
}
