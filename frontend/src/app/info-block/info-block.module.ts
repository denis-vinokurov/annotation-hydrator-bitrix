import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './components/list/list.component';
import {
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatProgressBarModule,
} from "@angular/material";
import {InfoBlockService} from "./services/info-block.service";
import {ElementComponent} from './components/element/element.component';
import {RouterModule, Routes} from "@angular/router";
import {SectionComponent} from './components/section/section.component';
import {SharedModule} from "../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {ListsComponent} from "../components/lists/lists.component";

const routes: Routes = [
    {
        path: 'info-block/:id',
        children: [
            {path: 'element', component: ElementComponent},
            {path: 'section', component: SectionComponent},
            {path: '', component: ListsComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), ReactiveFormsModule,

        SharedModule,

        MatListModule, MatFormFieldModule, MatButtonModule, MatCardModule, MatInputModule, MatExpansionModule,
        MatDialogModule, MatProgressBarModule,
    ],
    exports: [ListComponent, RouterModule],
    declarations: [ListComponent, ElementComponent, SectionComponent],
    providers: [InfoBlockService]
})
export class InfoBlockModule {
}
