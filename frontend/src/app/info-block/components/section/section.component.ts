import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {MatDialog} from "@angular/material";
import {FormControl, FormGroup} from "@angular/forms";
import {InfoBlockService} from "../../services/info-block.service";
import {TypeInterface} from "../../../shared/services/type-interface";
import {CodeViewerComponent} from "../../../shared/components/code-viewer/code-viewer.component";
import {SectionInterface} from "../../services/section";
import {FieldService} from "../../../shared/services/field.service";

@Component({
    selector: 'app-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.less']
})
export class SectionComponent implements OnInit {

    public item: SectionInterface = null;

    public types: Array<TypeInterface>;

    public form: FormGroup;

    constructor(private route: ActivatedRoute, private service: InfoBlockService, private dialog: MatDialog) {
    }

    onSubmit() {
        this.service.createEntity(this.form.value)
            .subscribe((answer: string) => {
                this.dialog.open(CodeViewerComponent, {
                    data: {
                        code: answer
                    }
                })
            });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.service.getById(parseInt(params.get('id'), 10))
                .subscribe((iBlock) => {
                    this.item = iBlock.section;
                    this.types = iBlock.section.types;

                    this.form = new FormGroup({
                        id: new FormControl(iBlock.id),
                        entityType: new FormControl('section'),
                        name: new FormControl(),
                        namespace: new FormControl(),
                        fields: new FormGroup(this.item.fields.reduce((acc, field) => {
                            acc[field.code] = FieldService.createGroup(field);
                            return acc;
                        }, {})),
                        properties: new FormGroup(this.item.properties.reduce((acc, field) => {
                            acc[field.code] = FieldService.createGroup(field);
                            return acc;
                        }, {})),
                    });
                })
        });
    }
}
