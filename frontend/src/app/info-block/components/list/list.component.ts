import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {InfoBlockService} from "../../services/info-block.service";
import {InfoBlockInterface} from "../../services/info-block";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-info-block-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
    protected fullList: Array<InfoBlockInterface> = [];

    public list: Array<InfoBlockInterface> = [];

    public filter: FormGroup;

    @Input('panelState')
    public panelState: EventEmitter<boolean>;

    constructor(private service: InfoBlockService) {
    }

    onInput() {
        const value = this.filter.get('name').value.toLocaleLowerCase();

        this.list = this.fullList.filter((el) => {
            return el.name.toLocaleLowerCase().indexOf(value) !== -1 ||
                el.id === parseInt(value, 10) ||
                el.code.toLocaleLowerCase().indexOf(value) !== -1;
        });
    }

    ngOnInit() {
        this.filter = new FormGroup({
            name: new FormControl(),
        });

        this.service.getList()
            .subscribe((list) => {
                this.fullList = list;
                this.list = list;
            })
    }

}
