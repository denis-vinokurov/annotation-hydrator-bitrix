import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {ElementInterface} from "../../services/element";
import {InfoBlockService} from "../../services/info-block.service";
import {TypeInterface} from "../../../shared/services/type-interface";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material";
import {CodeViewerComponent} from "../../../shared/components/code-viewer/code-viewer.component";
import {FieldService} from "../../../shared/services/field.service";

@Component({
    selector: 'app-element',
    templateUrl: './element.component.html',
    styleUrls: ['./element.component.less']
})
export class ElementComponent implements OnInit {
    public item: ElementInterface = null;

    public types: Array<TypeInterface>;

    public form: FormGroup;

    constructor(private route: ActivatedRoute, private service: InfoBlockService, private dialog: MatDialog) {
    }

    onSubmit() {
        this.service.createEntity(this.form.value)
            .subscribe((answer: string) => {
                this.dialog.open(CodeViewerComponent, {
                    data: {
                        code: answer
                    }
                })
            });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.service.getById(parseInt(params.get('id'), 10))
                .subscribe((iBlock) => {
                    this.item = iBlock.element;
                    this.types = iBlock.element.types;

                    this.form = new FormGroup({
                        id: new FormControl(iBlock.id),
                        entityType: new FormControl('element'),
                        name: new FormControl(),
                        namespace: new FormControl(),
                        fields: new FormGroup(iBlock.element.fields.reduce((acc, field) => {
                            acc[field.code] = FieldService.createGroup(field);
                            return acc;
                        }, {})),
                        properties: new FormGroup(iBlock.element.properties.reduce((acc, field) => {
                            acc[field.code] = FieldService.createGroup(field);
                            return acc;
                        }, {})),
                    });
                })
        });
    }
}
