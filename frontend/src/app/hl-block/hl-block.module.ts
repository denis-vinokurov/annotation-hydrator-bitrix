import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './components/list/list.component';
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {DetailComponent} from './components/detail/detail.component';
import {
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatProgressBarModule
} from "@angular/material";
import {ListsComponent} from "../components/lists/lists.component";

const routes: Routes = [
    {
        path: 'hl-block/:id',
        children: [
            {
                path: 'element',
                component: DetailComponent,
            },
            {
                path: '',
                component: ListsComponent,
            },
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), ReactiveFormsModule,

        SharedModule, MatListModule, MatFormFieldModule, MatButtonModule, MatInputModule, MatExpansionModule,
        MatCardModule, MatProgressBarModule,
    ],
    exports: [ListComponent, RouterModule],
    declarations: [ListComponent, DetailComponent]
})
export class HLBlockModule {
}
