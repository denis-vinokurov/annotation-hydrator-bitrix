import {FieldInterface} from "../../shared/services/field-interface";
import {TypeInterface} from "../../shared/services/type-interface";

export interface HLBlockInterface {
    id: number;
    name: string;
    code: string;

    types: Array<TypeInterface>;
    fields: Array<FieldInterface>;
}
