import {inject, TestBed} from '@angular/core/testing';

import {HlBlockService} from './hl-block.service';

describe('HlBlockService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [HlBlockService]
        });
    });

    it('should be created', inject([HlBlockService], (service: HlBlockService) => {
        expect(service).toBeTruthy();
    }));
});
