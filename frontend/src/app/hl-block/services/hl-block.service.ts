import {Injectable} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {HttpHelperService} from "../../shared/services/http-helper.service";
import {map} from "rxjs/operators";
import {DataService} from "../../shared/services/data.service";
import {HttpClient} from "@angular/common/http";
import {HLBlockInterface} from "./hl-block";

@Injectable({
    providedIn: 'root'
})
export class HLBlockService {

    public constructor(private dataService: DataService, private httpClient: HttpClient) {
    }

    public getList(): Observable<Array<HLBlockInterface>> {
        return this.dataService.fetch()
            .pipe(
                map(data => {
                    return data.HLBlocks;
                })
            );
    }

    getById(id: number): Observable<HLBlockInterface> {
        return this.getList()
            .pipe(
                map((items: Array<HLBlockInterface>) => {
                    return items.find((el) => el.id === id);
                })
            );
    }

    /**
     * @param {object} value
     * @returns {Observable<Object>}
     */
    createEntity(value: object) {
        return this.httpClient.post('/bitrix/admin/annotation-hydrator.php?type=hlBlock', HttpHelperService.serialize(value), {
            withCredentials: true,
            responseType: 'text',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        });
    }
}
