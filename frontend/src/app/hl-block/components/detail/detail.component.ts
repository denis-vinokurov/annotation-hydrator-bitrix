import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {MatDialog} from "@angular/material";
import {FormControl, FormGroup} from "@angular/forms";
import {TypeInterface} from "../../../shared/services/type-interface";
import {CodeViewerComponent} from "../../../shared/components/code-viewer/code-viewer.component";
import {HLBlockInterface} from "../../services/hl-block";
import {HLBlockService} from "../../services/hl-block.service";
import {FieldService} from "../../../shared/services/field.service";

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnInit {


    public item: HLBlockInterface = null;

    public types: Array<TypeInterface>;

    public form: FormGroup;

    constructor(private route: ActivatedRoute, private service: HLBlockService, private dialog: MatDialog) {
    }

    onSubmit() {
        this.service.createEntity(this.form.value)
            .subscribe((answer: string) => {
                this.dialog.open(CodeViewerComponent, {
                    data: {
                        code: answer
                    }
                })
            });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.service.getById(parseInt(params.get('id'), 10))
                .subscribe((hlBlock) => {
                    this.item = hlBlock;
                    this.types = hlBlock.types;

                    this.form = new FormGroup({
                        id: new FormControl(hlBlock.id),
                        name: new FormControl(),
                        namespace: new FormControl(),
                        fields: new FormGroup(this.item.fields.reduce((acc, field) => {
                            acc[field.code] = FieldService.createGroup(field);
                            return acc;
                        }, {})),
                    });

                })
        });
    }
}
