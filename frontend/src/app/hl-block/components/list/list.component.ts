import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {HLBlockInterface} from "../../services/hl-block";
import {HLBlockService} from "../../services/hl-block.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-hl-block-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
    public fullList: Array<HLBlockInterface> = [];
    public list: Array<HLBlockInterface> = [];
    public filter: FormGroup;

    @Input('panelState')
    public panelState: EventEmitter<boolean>;

    constructor(private service: HLBlockService) {
    }


    onInput() {
        const value = this.filter.get('name').value.toLocaleLowerCase();

        this.list = this.fullList.filter((el) => {
            return el.name.toLocaleLowerCase().indexOf(value) !== -1 ||
                el.id === parseInt(value, 10) ||
                el.code.toLocaleLowerCase().indexOf(value) !== -1;
        });
    }

    ngOnInit() {
        this.filter = new FormGroup({
            name: new FormControl(),
        });

        this.service.getList()
            .subscribe((list) => {
                this.fullList = list;
                this.list = list
            })
    }
}
