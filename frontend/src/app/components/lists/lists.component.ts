import {Component, OnInit, EventEmitter} from '@angular/core';
import {DataService} from "../../shared/services/data.service";

@Component({
    selector: 'app-lists',
    templateUrl: './lists.component.html',
    styleUrls: ['./lists.component.less']
})
export class ListsComponent implements OnInit {
    public ready = false;

    public infoBlockState: EventEmitter<boolean>;
    public hlBlockState: EventEmitter<boolean>;

    constructor(private service: DataService) {
    }

    ngOnInit() {
        this.infoBlockState = new EventEmitter();
        this.hlBlockState = new EventEmitter();

        this.service.fetch()
            .subscribe(() => {
                this.ready = true;
            });

    }
}
