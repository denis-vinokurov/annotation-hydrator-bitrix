import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FieldComponent} from "./components/field/field.component";
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatSelectModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {CodeViewerComponent} from "./components/code-viewer/code-viewer.component";
import {OptionsComponent} from './components/options/options.component';
import {FieldListComponent} from './components/field-list/field-list.component';
import {FocusDirective} from './directives/focus.directive';

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule,

        MatExpansionModule, MatSelectModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatButtonModule,
        MatDialogModule, MatInputModule, MatListModule, MatExpansionModule,
    ],
    exports: [FieldListComponent, FieldComponent, CodeViewerComponent, FocusDirective],
    declarations: [FieldComponent, CodeViewerComponent, OptionsComponent, FieldListComponent, FocusDirective],
    entryComponents: [CodeViewerComponent, OptionsComponent]
})

export class SharedModule {
}
