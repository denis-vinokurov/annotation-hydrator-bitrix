import {Directive, ElementRef, OnInit, Input, EventEmitter} from '@angular/core';

@Directive({
    selector: '[focus]'
})
export class FocusDirective implements OnInit {

    @Input('focus')
    public state: boolean | EventEmitter<boolean>;

    constructor(protected element: ElementRef<HTMLElement>) {
    }

    ngOnInit(): void {
        if (this.state instanceof EventEmitter) {
            this.state.subscribe(this.toggle.bind(this))
        } else {
            this.toggle(this.state)
        }
    }

    toggle(state) {
        setTimeout(() => {
            if (state) {
                this.element.nativeElement.focus();
            } else {
                this.element.nativeElement.blur();
            }
        }, 30);
    }

}
