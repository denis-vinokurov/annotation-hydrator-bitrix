import {InfoBlockInterface} from "../../info-block/services/info-block";
import {HLBlockInterface} from "../../hl-block/services/hl-block";

export interface DataInterface {
    iBlocks: Array<InfoBlockInterface>,
    HLBlocks: Array<HLBlockInterface>,
}