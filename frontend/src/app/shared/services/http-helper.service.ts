import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HttpHelperService {
    public static serialize(data: object, prefix = null) {
        const str = [];
        for (let [key, value] of Object.entries(data)) {
            if (data.hasOwnProperty(key)) {
                const name = prefix ? prefix + '[' + key + ']' : key;

                if (value === null || typeof value === "undefined") {
                    value = '';
                }

                str.push(
                    typeof value === 'object'
                        ? HttpHelperService.serialize(value, name)
                        : encodeURIComponent(name) + '=' + encodeURIComponent(value)
                );
            }
        }
        return str.join('&');
    }
}
