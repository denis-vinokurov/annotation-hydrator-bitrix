export interface FieldInterface {
    primary?: boolean;

    name: string;
    code: string;
    type: string;
    options: { [key: string]: any }

    defaultSelected: boolean;
}
