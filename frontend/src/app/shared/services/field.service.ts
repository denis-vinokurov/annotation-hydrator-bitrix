import {Injectable} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {FieldInterface} from "./field-interface";

@Injectable({
    providedIn: 'root'
})
export class FieldService {

    constructor() {
    }

    public static createGroup(field: FieldInterface): FormGroup {
        const optionControls = {};

        if (typeof field.options !== 'undefined' && field.options !== null) {
            for (let [key, value] of Object.entries(field.options)) {
                optionControls[key] = new FormControl(value);
            }
        }

        return new FormGroup({
            type: new FormControl(field.type),
            options: new FormGroup(optionControls),
        });
    }
}
