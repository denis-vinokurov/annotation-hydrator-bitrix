import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {DataInterface} from "./data-interface";
import {share} from "rxjs/operators";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class DataService {

    protected data: DataInterface | Observable<DataInterface> = null;

    constructor(protected httpClient: HttpClient) {
    }

    public fetch(): Observable<DataInterface> {
        if (this.data === null) {
            this.data = this.httpClient.post('/bitrix/admin/annotation-hydrator.php?type=data', null, {
                withCredentials: true
            })
                .pipe(share<DataInterface>());

            this.data.subscribe((data: DataInterface) => {
                this.data = data
            });

            return this.data;
        } else if (this.data instanceof Observable) {
            return this.data;
        } else {
            return Observable.create((observer) => {
                observer.next(this.data)
            });
        }
    }

}
