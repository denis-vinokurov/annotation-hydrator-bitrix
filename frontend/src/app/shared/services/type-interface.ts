export interface TypeInterface {
    name: string,
    code: string,
}
