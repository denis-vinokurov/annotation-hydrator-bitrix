import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";

@Component({
    selector: 'app-code-viewer',
    templateUrl: './code-viewer.component.html',
    styleUrls: ['./code-viewer.component.less']
})
export class CodeViewerComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: { code: string }) {
    }

    copy() {
        const tmp = document.createElement('TEXTAREA'); // Создаём новый текстовой input

        if (!(tmp instanceof HTMLTextAreaElement)) {
            return;
        }

        tmp.value = this.data.code; // Временному input вставляем текст для копирования
        document.body.appendChild(tmp); // Вставляем input в DOM
        tmp.select(); // Выделяем весь текст в input
        document.execCommand('copy'); // Магия! Копирует в буфер выделенный текст (см. команду выше)
        document.body.removeChild(tmp); // Удаляем временный input
    }

    ngOnInit() {
    }

}
