import {Component, Input, OnInit} from '@angular/core';
import {FieldInterface} from "../../services/field-interface";
import {AbstractControl, FormGroup} from "@angular/forms";
import {TypeInterface} from "../../services/type-interface";
import {MatCheckboxChange} from "@angular/material";

@Component({
    selector: 'app-field-list',
    templateUrl: './field-list.component.html',
    styleUrls: ['./field-list.component.less']
})
export class FieldListComponent implements OnInit {

    @Input()
    public title: string;

    @Input()
    public fields: Array<FieldInterface>;

    @Input()
    public formGroup: AbstractControl;

    @Input()
    public types: Array<TypeInterface>;


    public countSelected = 0;

    constructor() {
    }

    selectedField() {
        this.countSelected = this.fields.reduce((acc: number, field) => {
            if (!this.formGroup.get(field.code).disabled) {
                acc += 1;
            }
            return acc;
        }, 0);
    }

    changeOptionGroup(code: string, options: FormGroup) {
        const formGroup = this.formGroup.get(code);

        if (formGroup instanceof FormGroup) {
            formGroup.removeControl('options');
            formGroup.addControl('options', options)
        }
    }
    
    toggleAllFields(event: MatCheckboxChange) {
        this.fields.forEach((field) => {
            const group = this.formGroup.get(field.code);

            if (event.checked) {
                group.enable({
                    emitEvent: true
                });
            } else {
                group.disable({
                    emitEvent: true
                })
            }
        })
    }

    ngOnInit() {
        this.countSelected = this.fields
            .filter(field => field.defaultSelected)
            .length;
    }

}
