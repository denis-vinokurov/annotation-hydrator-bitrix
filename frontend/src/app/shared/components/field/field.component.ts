import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FieldInterface} from "../../services/field-interface";
import {TypeInterface} from "../../services/type-interface";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material";
import {OptionsComponent} from "../options/options.component";

@Component({
    selector: 'app-field',
    templateUrl: './field.component.html',
    styleUrls: ['./field.component.less']
})
export class FieldComponent implements OnInit {

    @Input()
    public field: FieldInterface;

    @Input()
    public types: Array<TypeInterface>;

    @Input()
    public formGroup: FormGroup;

    @Output()
    protected onSelected = new EventEmitter();

    @Output()
    protected onChangeOptionGroup = new EventEmitter();

    public selected: boolean;

    protected defaultType: string;
    protected optionsMap: { [key: string]: FormGroup } = {};

    protected options: FormGroup;

    constructor(private dialog: MatDialog) {
    }

    onSelect() {
        this.selected = !this.selected;
        this.toggleControl();
        this.onSelected.emit(this.selected);
    }

    onTypeChange() {
        let newFormGroup = null;
        const type = this.formGroup.get('type').value;

        if (typeof this.optionsMap[type] === 'undefined') {
            newFormGroup = new FormGroup({});
            this.optionsMap[type] = newFormGroup;
        }

        if (type === 'reference' && newFormGroup !== null) {
            newFormGroup.addControl('repository', new FormControl());
            newFormGroup.addControl('referenceField', new FormControl());
        }

        this.formGroup.addControl('options', this.optionsMap[type]);
        this.options = this.optionsMap[type];
        this.onChangeOptionGroup.emit(this.options);
    }

    toggleControl() {
        if (this.selected) {
            this.formGroup.enable({
                emitEvent: false
            });
        } else {
            this.formGroup.disable({
                emitEvent: false
            });
        }
    }

    showOptions() {
        this.dialog.open(OptionsComponent, {
            data: {
                options: this.options
            }
        });
    }

    isExistOptions(): boolean {
        return Object.values(this.options.controls).length > 0;
    }

    ngOnInit() {
        this.defaultType = this.formGroup.get('type').value;
        this.selected = this.field.defaultSelected;

        const formGroup = this.formGroup.get('options');
        if (!(formGroup instanceof FormGroup)) {
            return {};
        }

        this.optionsMap[this.formGroup.get('type').value] = formGroup;
        this.options = formGroup;

        this.toggleControl();
        
        this.formGroup.statusChanges.subscribe((state: any) => {
            this.selected = state !== 'DISABLED';
            this.onSelected.emit(this.selected);
        });
    }
}
