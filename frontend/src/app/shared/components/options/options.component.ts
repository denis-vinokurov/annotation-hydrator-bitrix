import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.less']
})
export class OptionsComponent implements OnInit {

    public nameMap = {
        repository: 'Репозиторий',
        referenceField: 'Код связующего поля',
    };

    constructor(@Inject(MAT_DIALOG_DATA) public data: { options: FormGroup }) {
    }

    ngOnInit() {

    }

    public getOptionKeys(): Array<string> {
        return Object.keys(this.data.options.controls);
    }
}
