import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListsComponent} from "../components/lists/lists.component";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {path: '', component: ListsComponent},
];


@NgModule({
    imports: [
        CommonModule, RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class RoutingModule {
}
