<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $APPLICATION->SetTitle('Annotation hydrator');

    \Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
    \Bitrix\Main\Page\Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">');
    ?>
    <app-annotation-hydrator-bitrix></app-annotation-hydrator-bitrix>
<?
    $dir = dir(__DIR__ . '/../dist/');

    while ($file = $dir->read()) {
        $pathInfo = pathinfo($dir->path . $file);
        if ($pathInfo['extension'] === 'js' && $pathInfo['basename'] !== 'main.js') {
            echo '<script>' . file_get_contents($dir->path . $file) . '</script>';
        }

        if ($pathInfo['extension'] === 'css') {
            \Bitrix\Main\Page\Asset::getInstance()->addString('<style>' . file_get_contents($dir->path . $file) . '</style>');
        }
    }
    echo '<script>' . file_get_contents($dir->path . 'main.js') . '</script>';


} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {

    global $APPLICATION;
    $APPLICATION->RestartBuffer();
    try {
        switch ($_GET['type']) {
            case 'data':
                echo new \Vinds\AnnotationHydratorBitrix\AdminSection\ViewModel();
                break;
            case 'iBlock':
                echo \Vinds\AnnotationHydratorBitrix\AdminSection\IBlockBuilder::build($_POST);
                break;
            case 'hlBlock':
                echo \Vinds\AnnotationHydratorBitrix\AdminSection\HigloadBlockBuilder::build($_POST);
                break;

        }
        die();
    } catch (\Vinds\AnnotationHydratorBitrix\Exception $e) {
        echo $e->getMessage();
    }
    die();

} ?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
