<?php

\Bitrix\Main\EventManager::getInstance()->addEventHandler('main', 'OnBuildGlobalMenu', function (&$adminMenu) {
    $adminMenu['global_menu_services']['items'][] = [
        'sort'      => 100,
        'url'       => '/bitrix/admin/annotation-hydrator.php',
        'text'      => 'Annotation hydrator',
        'title'     => 'Annotation hydrator',
        'icon'      => "form_menu_icon",
        'page_icon' => 'form_page_icon',
        'items_id'  => 'menu_webforms',
    ];
});

